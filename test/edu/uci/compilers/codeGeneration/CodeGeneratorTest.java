package edu.uci.compilers.codeGeneration;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;

import edu.uci.compilers.cfg.ControlFlowGraph;

public class CodeGeneratorTest extends TestCase{
	
	private CodeGenerator codeGenerator;
	
	@Before
	public void setUp(){
		ControlFlowGraph controlFlowGraph = null;
		codeGenerator = new CodeGenerator(controlFlowGraph);
	}
	
	@Test
	public void testGetF1Format(){
		int op = 15;
		int a = 10;
		int b = 5;
		int c = 10245;
		printBinary(op);
		printBinary(a);
		printBinary(b);
		printBinary(c);
		int f1Format = codeGenerator.getF1Format(op, a, b, c);
		printBinary(f1Format);
	}
	
	void printBinary(int i){
		System.out.println(Integer.toBinaryString(i));
	}
	
}
