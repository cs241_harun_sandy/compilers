package edu.uci.compilers.registerAllocator;

import java.io.File;
import java.io.IOException;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import edu.uci.compilers.cfg.ControlFlowGraph;
import edu.uci.compilers.cfg.IntermediateCodeGenerator;
import edu.uci.compilers.commonSubexpressionElimination.CommonSubexpressionElimination;
import edu.uci.compilers.copyPropagation.CopyPropagation;
import edu.uci.compilers.dataTypes.InterferenceGraph;
import edu.uci.compilers.debug.VCGGenerator;
import edu.uci.compilers.dominatorTree.DominatorTree;
import edu.uci.compilers.dominatorTree.DominatorTreeGenerator;
import edu.uci.compilers.parser.Parser;
import junit.framework.TestCase;

public class LiveRangeAnalyzerTest extends TestCase{
	private Parser parser;
	private String basePath;
	private String fileName;
	private ControlFlowGraph cfg;
	private DominatorTreeGenerator domTree;
	private Set<DominatorTree> domTrees;
	private CopyPropagation copyProp;
	private VCGGenerator vcgCfg, vcgDomTree, vcgCopyPropOne, vcgCopyPropTwo, vcgCse, lraVcg;
	private CommonSubexpressionElimination cse;
	private LiveRangeAnalyzer lra;
	private InterferenceGraph ifg;
	@Before
	public void setUp() throws IOException{
		basePath = "resources/PL241/";
		String file = "/smallLiveRangeAnalysis.txt";
		fileName = basePath+file;
		parser = new Parser(fileName);
		parser.computation();
		String vcgBase = "vcg/"+"/smallLiveRangeAnalysis/";
		File dir = new File(vcgBase);
		dir.mkdir();
		IntermediateCodeGenerator icg = new IntermediateCodeGenerator();
		vcgCopyPropOne = new VCGGenerator(vcgBase+"copyPropOne.vcg");
		vcgCfg = new VCGGenerator(vcgBase+"cfg.vcg");
		vcgCse = new VCGGenerator(vcgBase+"cse.vcg");
		vcgCopyPropTwo = new VCGGenerator(vcgBase+"copyPropTwo.vcg");
		vcgDomTree = new VCGGenerator(vcgBase+"dominatorTree.vcg");
		cfg = parser.getCfg();
		System.out.println("The CFG:----------------------");
		cfg.printCFG();
		System.out.println("------------------------------");
		domTree = new DominatorTreeGenerator(cfg);
		domTree.makeDominators();
		domTree.processAllBlockVsDominators();
		domTrees = domTree.buildDominatorTrees();
		vcgDomTree.generateVcgForDominatorTree(domTrees);
		copyProp = new CopyPropagation(domTrees);
		cse = new CommonSubexpressionElimination(domTrees);
		ifg = new InterferenceGraph();
		lraVcg = new VCGGenerator(vcgBase + "lra.vcg");
	}
	@Test
	public void test() throws IOException{
		vcgCfg.generateVcgForCfg(cfg);
		copyProp.makeAllCopyPropagations();
		vcgCopyPropOne.generateVcgForCfg(cfg);
		cse.doCommonSubexpressionElimination();
		while(!cse.getIsCseDone()){
			copyProp.makeAllCopyPropagations();//to get rid of prop statements
			cse.doCommonSubexpressionElimination();
		}
		vcgCse.generateVcgForCfg(cfg);
		vcgCopyPropTwo.generateVcgForCfg(cfg);
		lra = new LiveRangeAnalyzer();
		lra.setInterferenceGraph(ifg);
		lra.setCfg(cfg);
		lra.setBackEdge(false);
		lra.buildInterferenceGraph(cfg);
		lraVcg.generateVcgForInterferenceGraph(lra.getInterferenceGraph());
		lra.getInterferenceGraph().printAdjList();
		System.out.println("Live Range Analysis done");
	}
}
