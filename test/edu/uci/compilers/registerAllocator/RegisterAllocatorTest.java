package edu.uci.compilers.registerAllocator;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import edu.uci.compilers.cfg.ControlFlowGraph;
import edu.uci.compilers.cfg.IntermediateCodeGenerator;
import edu.uci.compilers.commonSubexpressionElimination.CommonSubexpressionElimination;
import edu.uci.compilers.copyPropagation.CopyPropagation;
import edu.uci.compilers.dataTypes.InterferenceGraph;
import edu.uci.compilers.dataTypes.Result;
import edu.uci.compilers.dataTypes.Result.Type;
import edu.uci.compilers.debug.VCGGenerator;
import edu.uci.compilers.dominatorTree.DominatorTree;
import edu.uci.compilers.dominatorTree.DominatorTreeGenerator;
import edu.uci.compilers.parser.Parser;
import junit.framework.TestCase;

public class RegisterAllocatorTest extends TestCase{
	private Parser parser;
	private String basePath;
	private String fileName;
	private ControlFlowGraph cfg;
	private DominatorTreeGenerator domTree;
	private Set<DominatorTree> domTrees;
	private CopyPropagation copyProp;
	private VCGGenerator vcgCfg, vcgDomTree, vcgCopyPropOne, vcgCopyPropTwo, vcgCse, lraVcg, regAllocVcg;
	private CommonSubexpressionElimination cse;
	private LiveRangeAnalyzer lra;
	private InterferenceGraph ifg;
	private RegisterAllocator regAlloc;
	
	@Before
	public void setUp() throws IOException{
		basePath = "resources/PL241/";
		String file = "/cell.txt";
		String vcgBase = "vcg/"+"/cell/";
		File dir = new File(vcgBase);
		dir.mkdir();
		fileName = basePath+file;
		IntermediateCodeGenerator icg = new IntermediateCodeGenerator();
		parser = new Parser(fileName);
		parser.computation();
		cfg = parser.getCfg();
		System.out.println("The CFG:----------------------");
		cfg.printCFG();
		System.out.println("------------------------------");
		vcgCfg = new VCGGenerator(vcgBase+"cfg.vcg");
		vcgCfg.generateVcgForCfg(cfg);
		vcgCse = new VCGGenerator(vcgBase+"cse.vcg");
		vcgCopyPropTwo = new VCGGenerator(vcgBase+"copyPropTwo.vcg");
		vcgDomTree = new VCGGenerator(vcgBase+"dominatorTree.vcg");
		lraVcg = new VCGGenerator(vcgBase + "lra.vcg");
		domTree = new DominatorTreeGenerator(cfg);
		domTree.makeDominators();
		domTree.processAllBlockVsDominators();
		domTrees = domTree.buildDominatorTrees();
		vcgDomTree.generateVcgForDominatorTree(domTrees);
		copyProp = new CopyPropagation(domTrees);
		vcgCopyPropOne = new VCGGenerator(vcgBase+"copyPropOne.vcg");
		vcgCopyPropOne.generateVcgForCfg(cfg);
		cse = new CommonSubexpressionElimination(domTrees);
		ifg = new InterferenceGraph();
		lraVcg = new VCGGenerator(vcgBase + "lra.vcg");
		copyProp.makeAllCopyPropagations();
		cse.doCommonSubexpressionElimination();
		while(!cse.getIsCseDone()){
			copyProp.makeAllCopyPropagations();
			cse.doCommonSubexpressionElimination();
		}
		vcgCse.generateVcgForCfg(cfg);
		regAlloc = new RegisterAllocator();
		lra = new LiveRangeAnalyzer();
		lra.setBackEdge(true);
		lra.buildInterferenceGraph(cfg);
//		lra.getInterferenceGraph().printAdjList();
		lraVcg.generateVcgForInterferenceGraph(lra.getInterferenceGraph());
		regAllocVcg = new VCGGenerator(vcgBase+"regAlloc.vcg");
		
	}
	@Test
	public void test() throws IOException{
		regAlloc.setCfg(cfg);
		regAlloc.allocateRegisters(lra.getInterferenceGraph());
//		regAlloc.printRegisterMap();
		regAllocVcg.generateVcgForCfg(cfg);
		System.out.println("Register Allocation done");
	}
}
