package edu.uci.compilers.debug;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import edu.uci.compilers.cfg.ControlFlowGraph;
import edu.uci.compilers.cfg.IntermediateCodeGenerator;
import edu.uci.compilers.dataTypes.BasicBlock;
import edu.uci.compilers.dataTypes.Instruction;
import edu.uci.compilers.parser.Parser;
import junit.framework.TestCase;

public class VCGGeneratorTest extends TestCase{

	private Parser parser;
	private String basePath;
	private String fileName;
	private VCGGenerator vcgGen;
	private ControlFlowGraph cfg;
	@Before
	public void setUp() throws IOException{
		basePath = "resources/PL241/";
		fileName = basePath+"/test001.txt";
		parser = new Parser(fileName);
		vcgGen = new VCGGenerator();
//		cfg = new ControlFlowGraph();
		IntermediateCodeGenerator icg = new IntermediateCodeGenerator();
//		parser.setIcg(icg);
//		parser.setCfg(cfg);
	}
	@Test
	public void testVcg() throws IOException {
		parser.computation();
		cfg=parser.getCfg();
		vcgGen.generateVcgForCfg(cfg);
		System.out.println("Test VCG done. Check VCG file");
	}

}
