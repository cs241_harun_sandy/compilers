package edu.uci.compilers.copyPropagation;

import java.io.IOException;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import edu.uci.compilers.cfg.ControlFlowGraph;
import edu.uci.compilers.cfg.IntermediateCodeGenerator;
import edu.uci.compilers.debug.VCGGenerator;
import edu.uci.compilers.debug.VCGGeneratorTest;
import edu.uci.compilers.dominatorTree.DominatorTree;
import edu.uci.compilers.dominatorTree.DominatorTreeGenerator;
import edu.uci.compilers.parser.Parser;
import junit.framework.TestCase;

public class CopyPropagationTest extends TestCase{

	private Parser parser;
	private String basePath;
	private String fileName;
	private ControlFlowGraph cfg;
	private DominatorTreeGenerator domTree;
	private VCGGenerator vcgCopyProp;
	private Set<DominatorTree> domTrees;
	private CopyPropagation copyProp;
	private VCGGenerator vcgCfg, vcgDomTree;
	@Before
	public void setUp() throws IOException{
		basePath = "resources/PL241/";
		fileName = basePath+"/test001.txt";
		parser = new Parser(fileName);
		parser.computation();
		IntermediateCodeGenerator icg = new IntermediateCodeGenerator();
		vcgCopyProp = new VCGGenerator("copyPropagation.vcg");
		vcgCfg = new VCGGenerator("cfg.vcg");
		vcgDomTree = new VCGGenerator("dominatorTree.vcg");
		cfg = parser.getCfg();
		System.out.println("The CFG:----------------------");
		cfg.printCFG();
		System.out.println("------------------------------");
		domTree = new DominatorTreeGenerator(cfg);
		domTree.makeDominators();
		domTree.processAllBlockVsDominators();
		domTrees = domTree.buildDominatorTrees();
		vcgDomTree.generateVcgForDominatorTree(domTrees);
		vcgCfg.generateVcgForCfg(cfg);
		copyProp = new CopyPropagation(domTrees);
	}
	
	@Test
	public void testCopyPropagation() throws IOException{
		System.out.println("Number of dominator trees: "+ domTrees.size());
		copyProp.makeAllCopyPropagations();
		System.out.println("Copy Propagation done");
		vcgCopyProp.generateVcgForCfg(cfg);
	}
}
