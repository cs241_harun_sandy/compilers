package edu.uci.compilers.dominatorTree;

import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.junit.Before;
import org.junit.Test;

import edu.uci.compilers.cfg.ControlFlowGraph;
import edu.uci.compilers.cfg.IntermediateCodeGenerator;
import edu.uci.compilers.dataTypes.BasicBlock;
import edu.uci.compilers.debug.VCGGenerator;
import edu.uci.compilers.parser.Parser;
import junit.framework.TestCase;

public class DominatorTreeTest extends TestCase{
	private Parser parser;
	private String basePath;
	private String fileName;
	private ControlFlowGraph cfg;
	private DominatorTreeGenerator domTree;
	private VCGGenerator vcg;

	@Before
	public void setUp() throws IOException{
		basePath = "resources/PL241/";
		fileName = basePath+"/test001.txt";
		parser = new Parser(fileName);
		IntermediateCodeGenerator icg = new IntermediateCodeGenerator();
		vcg = new VCGGenerator("dominatorTree.vcg");
	}
	@Test
	public void testDominatorTree() throws IOException {
		parser.computation();
		cfg=parser.getCfg();
		domTree = new DominatorTreeGenerator(cfg);
		domTree.makeDominators();
		domTree.processAllBlockVsDominators();
		Set<DominatorTree> domTrees = domTree.buildDominatorTrees();
		for(DominatorTree domTree: domTrees){
			System.out.println("################################");
			domTree.printDominatorTree();
			System.out.println("################################");
		}
		vcg.generateVcgForDominatorTree(domTrees);
		System.out.println("Dominator Tree generated.");
	}

}
