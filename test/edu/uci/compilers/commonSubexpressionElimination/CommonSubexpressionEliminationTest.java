package edu.uci.compilers.commonSubexpressionElimination;

import java.io.IOException;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import edu.uci.compilers.cfg.ControlFlowGraph;
import edu.uci.compilers.cfg.IntermediateCodeGenerator;
import edu.uci.compilers.copyPropagation.CopyPropagation;
import edu.uci.compilers.debug.VCGGenerator;
import edu.uci.compilers.dominatorTree.DominatorTree;
import edu.uci.compilers.dominatorTree.DominatorTreeGenerator;
import edu.uci.compilers.parser.Parser;
import junit.framework.TestCase;

public class CommonSubexpressionEliminationTest extends TestCase{
	private Parser parser;
	private String basePath;
	private String fileName;
	private ControlFlowGraph cfg;
	private DominatorTreeGenerator domTree;
	private Set<DominatorTree> domTrees;
	private VCGGenerator vcgCfg, vcgDomTree, cseVcg;
	private CommonSubexpressionElimination cse;
	@Before
	public void setUp() throws IOException{
		basePath = "resources/PL241/";
		fileName = basePath+"/test001.txt";
		parser = new Parser(fileName);
		parser.computation();
		IntermediateCodeGenerator icg = new IntermediateCodeGenerator();
		vcgCfg = new VCGGenerator("cfg.vcg");
		vcgDomTree = new VCGGenerator("dominatorTree.vcg");
		cfg = parser.getCfg();
		System.out.println("The CFG:----------------------");
		cfg.printCFG();
		System.out.println("------------------------------");
		domTree = new DominatorTreeGenerator(cfg);
		domTree.makeDominators();
		domTree.processAllBlockVsDominators();
		domTrees = domTree.buildDominatorTrees();
		vcgDomTree.generateVcgForDominatorTree(domTrees);
		vcgCfg.generateVcgForCfg(cfg);
		cseVcg = new VCGGenerator("cseVcg.vcg");
		cse = new CommonSubexpressionElimination(domTrees);
	}
	
	@Test
	public void testCommonSubexpressionElimination() throws IOException{
		cse.doCommonSubexpressionElimination();
		cseVcg.generateVcgForCfg(parser.getCfg());
		System.out.println("CSE Test done");
	}

}
