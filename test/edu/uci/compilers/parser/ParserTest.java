package edu.uci.compilers.parser;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import edu.uci.compilers.cfg.ControlFlowGraph;
import edu.uci.compilers.cfg.IntermediateCodeGenerator;
import edu.uci.compilers.dataTypes.BasicBlock;
import edu.uci.compilers.dataTypes.Instruction;
import edu.uci.compilers.dataTypes.Token;
import junit.framework.TestCase;

public class ParserTest extends TestCase{
	
	private Parser parser;
	private String fileName;
	private String basePath;
	
	@Before
	public void setUp() throws IOException{
		basePath = "resources/PL241/";
		fileName = basePath+"/test001.txt";
		parser = new Parser(fileName);
//		ControlFlowGraph cfg = new ControlFlowGraph();
//		IntermediateCodeGenerator icg = new IntermediateCodeGenerator();
//		parser.setIcg(icg);
//		parser.setCfg(cfg);
	}
	
	@Test
	public void testComputation() throws IOException{
		System.out.println("Testing start");
		parser.computation();
		System.out.println("Test done");
	}
	
	@Test
	public void testNext() throws IOException{
		Token token = null;
		while((token = parser.getToken()) != Token.EOF){
			parser.moveToNextToken();
			System.out.println(token);
		}
	}
	
	@Test
	public void testComputation_multiple() throws IOException{
		File folder = new File(basePath);
		File[] files = folder.listFiles();
		for(File file : files){
			
			String fileName = file.getAbsolutePath();
			System.out.println("Testing with the file:"+file.getName());
			Parser parser = new Parser(fileName);
			parser.computation();
			if(parser.getParseStatus()==false){
				System.out.println("Parse Fail\n");
			}else{
				System.out.println("Parsing Completed\n");				
			}
		}
	}
	
	
	
}
