package edu.uci.compilers.parser;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import junit.framework.TestCase;

public class ReaderTest extends TestCase{
	
	private Reader reader;
	private String fileName;
	
	@Before
	public void setUp(){
		fileName = "PL241/test001.txt";
		reader = new Reader(fileName);
	}
	
	@Test
	public void testGetSym() throws IOException{
		char sample = reader.getSym();
	}
	
	@Test
	public void testConsumeLine() throws IOException{
		reader.consumeLine();
	}
}
