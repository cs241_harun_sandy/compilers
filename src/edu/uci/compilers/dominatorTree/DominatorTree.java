package edu.uci.compilers.dominatorTree;

import java.util.HashSet;

import edu.uci.compilers.dataTypes.BasicBlock;

public class DominatorTree {
	private DominatorTreeNode root;
	
	public DominatorTree(DominatorTreeNode rootNode){
		this.root = rootNode;
	}
	
	public void printDominatorTree(){
		printDominatorTreeUtil(root);
	}
	
	public DominatorTreeNode getRoot() {
		return root;
	}
	
	private void printDominatorTreeUtil(DominatorTreeNode node){
		System.out.print(node.getBasicBlock().getBasicBlockId()+" = {");
		for(DominatorTreeNode child: node.getChildren()){
			System.out.print(child.getBasicBlock().getBasicBlockId()+", ");
		}
		System.out.println("}");
		for(DominatorTreeNode child: node.getChildren()){
			printDominatorTreeUtil(child);
		}
	}
}
