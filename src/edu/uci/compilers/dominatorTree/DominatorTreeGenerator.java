package edu.uci.compilers.dominatorTree;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.util.Queue;

import edu.uci.compilers.cfg.ControlFlowGraph;
import edu.uci.compilers.dataTypes.BasicBlock;
import edu.uci.compilers.dataTypes.Function;
import edu.uci.compilers.dataTypes.GlobalVariableManager;
/**
 * First call makeDominators and then buildDominatorTrees
 * @author harun
 *
 */
public class DominatorTreeGenerator {
	private DominatorTreeNode root;
	private Map<BasicBlock, Set<BasicBlock>> blockVsDominators;
	private GlobalVariableManager gvm;
	private HashSet<Map<BasicBlock, Set<BasicBlock>>> allBlockVsDominators;
	private HashSet<DominatorTreeGenerator> dominatorTrees; 
		
	public Map<BasicBlock, Set<BasicBlock>> getBlockVsDominators() {
		return blockVsDominators;
	}
	public void printBlockVsDominatorsForVcg(){
		for(Map<BasicBlock, Set<BasicBlock>> map: allBlockVsDominators){
			System.out.println("-----------------------");
			for(Entry<BasicBlock, Set<BasicBlock>> keyVal: map.entrySet()){
				System.out.print("["+keyVal.getKey().getBasicBlockId()+"]: {");
				for(BasicBlock bbl: keyVal.getValue()){
					System.out.print("["+bbl.getBasicBlockId()+"], ");
				}
				System.out.print("}\n");
			}
		}
	}
	
	public void printBlockVsDominators(){
		for(Entry<BasicBlock, Set<BasicBlock>> keyVal: blockVsDominators.entrySet()){
			BasicBlock key = keyVal.getKey();
			Set<BasicBlock> val = keyVal.getValue();
			System.out.print("["+key.getBasicBlockId()+"]: {");
			for(BasicBlock child: val){
				System.out.print("["+child.getBasicBlockId()+"],");
			}
			System.out.println("}");
		}

	}
	private void dfsInit(BasicBlock basicBlock, HashSet<BasicBlock> visited){
		if(!visited.contains(basicBlock)){
			visited.add(basicBlock);
			HashSet<BasicBlock> dominators = new HashSet<BasicBlock>();
			blockVsDominators.put(basicBlock, dominators);
			for(BasicBlock bbl: basicBlock.getChildren()){
				dfsInit(bbl, visited);
			}
		}
	}
	
	private void dfsIgnoringRemovedBasicBlock(BasicBlock basicBlock, BasicBlock removedBasicBlock, HashSet<BasicBlock> visited){
		if(!visited.contains(basicBlock) && basicBlock.getBasicBlockId() != removedBasicBlock.getBasicBlockId()){
			visited.add(basicBlock);
			for(BasicBlock bbl: basicBlock.getChildren()){
				dfsIgnoringRemovedBasicBlock(bbl, removedBasicBlock, visited);
			}
		}
	}
	
	private HashSet<BasicBlock> findReachableWithoutRemovedBasicBlock(BasicBlock rootBasicBlock, BasicBlock removedBasicBlock){
		HashSet<BasicBlock> visited = new HashSet<BasicBlock>();
		dfsIgnoringRemovedBasicBlock(rootBasicBlock, removedBasicBlock, visited);
		return visited;
	}
	
	public Set<DominatorTree> buildDominatorTrees(){
		processAllBlockVsDominators();
		Set<DominatorTree> dominatorTrees = new HashSet<DominatorTree>();
		for(Map<BasicBlock, Set<BasicBlock>> blockVsDoms: allBlockVsDominators){
			Queue<DominatorTreeNode> domTreeQ = new LinkedList<DominatorTreeNode>();
			BasicBlock rootBasicBlock = findRootBasicBlock(blockVsDoms);
			DominatorTreeNode rootDominatorTreeNode = new DominatorTreeNode(rootBasicBlock);
			domTreeQ.add(rootDominatorTreeNode);
			
			HashSet<DominatorTreeNode> visited = new HashSet<DominatorTreeNode>();
			//standard BFS to build the tree
			while(!domTreeQ.isEmpty()){
				DominatorTreeNode domTreeNode = domTreeQ.poll();
				if(visited.contains(domTreeNode)){
					continue;
				}
				visited.add(domTreeNode);
				Set<DominatorTreeNode> children = getChildrenOfDominatorTreeNode(domTreeNode, blockVsDoms);
//				System.out.println("Before removing "+domTreeNode.getBasicBlock().getBasicBlockId());
//				printBlockVsDominators();
				removeFromDominators(domTreeNode, blockVsDoms);
//				System.out.println("After removing "+domTreeNode.getBasicBlock().getBasicBlockId());
//				printBlockVsDominators();
				domTreeNode.setChildren(children);
				for(DominatorTreeNode child: children){
					child.setParent(domTreeNode);
				}
				for(DominatorTreeNode node: children){
					domTreeQ.add(node);
				}
			}
			
			DominatorTree domTree = new DominatorTree(rootDominatorTreeNode);
			dominatorTrees.add(domTree);
		}
		return dominatorTrees;
	}
	
	private void removeFromDominators(DominatorTreeNode domTreeNode, Map<BasicBlock, Set<BasicBlock>> blockVsDoms) {
		for(Entry<BasicBlock, Set<BasicBlock>> keyVal: blockVsDoms.entrySet()){
			Set<BasicBlock> dominators = keyVal.getValue();
			dominators.remove(domTreeNode.getBasicBlock());
		}
	}
	private Set<DominatorTreeNode> getChildrenOfDominatorTreeNode(DominatorTreeNode domTreeNode, 
			Map<BasicBlock, Set<BasicBlock>> blockVsDoms) {
		//all nodes that are dominated ONLY by domTreeNode are the children of domTreeNode
		Set<DominatorTreeNode> children = new HashSet<DominatorTreeNode>();
		for(Entry<BasicBlock, Set<BasicBlock>> keyVal: blockVsDoms.entrySet()){
			BasicBlock childCandidate = keyVal.getKey();
			Set<BasicBlock> dominators = keyVal.getValue();
			if(dominators.size() == 1 && dominators.contains(domTreeNode.getBasicBlock())){//this is a child
				children.add(new DominatorTreeNode(childCandidate));
			}
		}
		return children;
	}
	public void processAllBlockVsDominators() {
		//processes the map an removes the entries that are dominated by itself. 
		//for ex. basicBlockX dominates {basicBlockX, basicBlockY, basicBlockZ} gets changed to
		//basicBlockX dominates {basicBlockY, basicBlockZ}
		HashSet<Map<BasicBlock, Set<BasicBlock>>> newAllBlockVsDominators = 
				new HashSet<Map<BasicBlock, Set<BasicBlock>>>();
		for(Map<BasicBlock, Set<BasicBlock>> blockVsDoms: allBlockVsDominators){
			Map<BasicBlock, Set<BasicBlock>> newBlockVsDoms = new HashMap<BasicBlock, Set<BasicBlock>>();
			for(Entry<BasicBlock, Set<BasicBlock>> keyVal: blockVsDoms.entrySet()){
				BasicBlock basicBlock = keyVal.getKey();
				Set<BasicBlock> dominators = keyVal.getValue();
				dominators.remove(basicBlock);
				newBlockVsDoms.put(basicBlock, dominators);
			}
			newAllBlockVsDominators.add(newBlockVsDoms);
		}
		allBlockVsDominators = newAllBlockVsDominators;
		
	}
	private BasicBlock findRootBasicBlock(Map<BasicBlock, Set<BasicBlock>> blockVsDoms) {
		//rootBasicBlock is the one which is not dominated by anything else. 
		//remember processAllBlockVsDominators removes same domination i.e., (X dominates itself)
		BasicBlock rootBasicBlock = null;
		for(Entry<BasicBlock, Set<BasicBlock>> keyVal: blockVsDoms.entrySet()){
			BasicBlock rootCandidate = keyVal.getKey();
			Set<BasicBlock> dominators = keyVal.getValue();
			if(dominators.size() == 0){//else it cannot be a root as root is dominated only by itself
				rootBasicBlock = rootCandidate;
				break;
			}
		}
		return rootBasicBlock;
	}

	public void makeDominators(){
		BasicBlock mainBasicBlock = root.getBasicBlock();
		HashSet<BasicBlock> allBasicBlocks = new HashSet<BasicBlock>();
		blockVsDominators.clear();
		dfsInit(mainBasicBlock, allBasicBlocks);
		HashSet<BasicBlock> maintainVisited = new HashSet<BasicBlock>();
		dfsDominator(mainBasicBlock, maintainVisited, allBasicBlocks, root.getBasicBlock());
		allBlockVsDominators.add(deepCopy(blockVsDominators));
		for(Entry<String, Function> func: gvm.getFunctions().entrySet()){
			blockVsDominators.clear();
			HashSet<BasicBlock> allBasicBlocksForFunction = new HashSet<BasicBlock>();
			BasicBlock funcBasicBlock = func.getValue().getStartBlock();
			dfsInit(funcBasicBlock, allBasicBlocksForFunction);
			HashSet<BasicBlock> maintainVisitedForFunc = new HashSet<BasicBlock>();
			dfsDominator(funcBasicBlock, maintainVisitedForFunc, allBasicBlocksForFunction, funcBasicBlock);
			allBlockVsDominators.add(deepCopy(blockVsDominators));
		}
	}
	
	private static Map<BasicBlock, Set<BasicBlock>> deepCopy(Map<BasicBlock, Set<BasicBlock>> blockVsDominators2) {
		Map<BasicBlock, Set<BasicBlock>> deepCopied = new HashMap<BasicBlock, Set<BasicBlock>>();
		for(Entry<BasicBlock, Set<BasicBlock>> keyVal: blockVsDominators2.entrySet()){
			deepCopied.put(keyVal.getKey(), keyVal.getValue());
		}
		return deepCopied;
	}

	private void dfsDominator(BasicBlock basicBlock, HashSet<BasicBlock> visited, HashSet<BasicBlock> allBasicBlocks,
			BasicBlock rootBasicBlock) {
		if(!visited.contains(basicBlock)){
			visited.add(basicBlock);
			HashSet<BasicBlock> reachableBasicBlocks = findReachableWithoutRemovedBasicBlock(rootBasicBlock, basicBlock);
			for(BasicBlock bbl: allBasicBlocks){
				if(!reachableBasicBlocks.contains(bbl)){//basicBlock dominates bbl
					Set<BasicBlock> dominators = blockVsDominators.get(bbl);
					dominators.add(basicBlock);
					blockVsDominators.put(bbl, dominators);
				}
			}
			for(BasicBlock bbl: basicBlock.getChildren()){
				dfsDominator(bbl, visited, allBasicBlocks, rootBasicBlock);
			}
		}
	}

	private void initializeBlockVsDominators(BasicBlock basicBlock){
		HashSet<BasicBlock> visited = new HashSet<BasicBlock>();
		dfsInit(basicBlock, visited);
		for(Entry<String, Function> func: gvm.getFunctions().entrySet()){
			dfsInit(func.getValue().getStartBlock(), visited);
		}
//		allBasicBlocks = visited;
	}
	public DominatorTreeGenerator(ControlFlowGraph cfg){
		root = new DominatorTreeNode();
		gvm = GlobalVariableManager.getInstance();
		root.setBasicBlock(cfg.getStartBlock());
		blockVsDominators = new HashMap<BasicBlock, Set<BasicBlock>>();
		allBlockVsDominators = new HashSet<Map<BasicBlock, Set<BasicBlock>>>();
		initializeBlockVsDominators(cfg.getStartBlock());
	}
	
}
