package edu.uci.compilers.dominatorTree;

import java.util.HashSet;
import java.util.Set;

import edu.uci.compilers.dataTypes.BasicBlock;

public class DominatorTreeNode {
	private BasicBlock basicBlock;
	private Set<DominatorTreeNode> children;
	private DominatorTreeNode parent;
	
	public DominatorTreeNode(){}
	
	public DominatorTreeNode(BasicBlock basicBlock){
		this.basicBlock = basicBlock;
		this.children = new HashSet<DominatorTreeNode>();
	}
	
	public BasicBlock getBasicBlock() {
		return basicBlock;
	}
	public void setBasicBlock(BasicBlock basicBlock) {
		this.basicBlock = basicBlock;
	}
	public Set<DominatorTreeNode> getChildren() {
		return children;
	}
	public void setChildren(Set<DominatorTreeNode> children) {
		this.children = children;
	}
	public DominatorTreeNode getParent() {
		return parent;
	}
	public void setParent(DominatorTreeNode parent) {
		this.parent = parent;
	}
	public String toString(){
		return basicBlock.getBasicBlockId()+"";
	}
	
}
