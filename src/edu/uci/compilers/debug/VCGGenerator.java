package edu.uci.compilers.debug;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import edu.uci.compilers.cfg.ControlFlowGraph;
import edu.uci.compilers.dataTypes.BasicBlock;
import edu.uci.compilers.dataTypes.Function;
import edu.uci.compilers.dataTypes.GlobalVariableManager;
import edu.uci.compilers.dataTypes.Instruction;
import edu.uci.compilers.dataTypes.InterferenceGraph;
import edu.uci.compilers.dataTypes.Result;
import edu.uci.compilers.dominatorTree.DominatorTree;
import edu.uci.compilers.dominatorTree.DominatorTreeNode;

public class VCGGenerator {
	private FileWriter vcgWriter;
	
	public VCGGenerator() throws IOException{
		vcgWriter = new FileWriter("testVCG.vcg");
	}
	
	public VCGGenerator(String fileName) throws IOException{
		vcgWriter = new FileWriter(fileName);
	}
	
	public void generateVcgForCfg(ControlFlowGraph cfg) throws IOException{
		HashSet<Integer> visited = new HashSet<Integer>();
		vcgWriter.write("graph: { title: \"Control Flow Graph\"\n"
				+ "layoutalgorithm: dfs\n"
				+ "manhattan_edges: yes\n"
				+ "smanhattan_edges: yes\n");
		generateVcgUtil(cfg.getStartBlock(), visited);
		generateVcgForFunctionsUtil(GlobalVariableManager.getInstance().getFunctions());;
		vcgWriter.write("}");
		vcgWriter.flush();
		vcgWriter.close();
	}

	private void generateVcgForFunctionsUtil(Map<String, Function> functions) throws IOException{
		for(Entry<String, Function> keyVal: functions.entrySet()){
			HashSet<Integer> visited = new HashSet<Integer>();
			generateVcgUtil(keyVal.getValue().getStartBlock(), visited);
		}
	}
	
	private void generateVcgUtil(BasicBlock basicBlock, HashSet<Integer> visited) throws IOException {
		int id=basicBlock.getBasicBlockId();
		if(!visited.contains(id)){
			visited.add(id);
			//first write header
			vcgWriter.write("node: {\n"
					+ "title: \""+id+"\"\n"
					+ "label: \""+id+", "+basicBlock.getType()+
					"[\n");
			//write instructions
			for(Instruction instr: basicBlock.getInstructions()){
				vcgWriter.write(instr.toString()+";\n");
			}
			for(BasicBlock parent: basicBlock.getParents()){
				vcgWriter.write("parent: " + parent.getBasicBlockId() + ";\n");
			}
			if(basicBlock.getParents() == null || basicBlock.getParents().isEmpty()){
				vcgWriter.write("parents: null\n");
			}
			for(BasicBlock child: basicBlock.getChildren()){
				vcgWriter.write("child: " + child.getBasicBlockId() + ";\n");
			}
			vcgWriter.write("]\"\n"
					+ "}\n");
			//write edges
			for(BasicBlock bblock: basicBlock.getChildren()){
				vcgWriter.write("edge: { sourcename: \""+id+"\"\n"
						+ "targetname: \""+bblock.getBasicBlockId()+"\"\n"
								+ "color: blue\n"
								+ "}\n");
			}
			vcgWriter.flush();
			for(BasicBlock bblock: basicBlock.getChildren()){
				generateVcgUtil(bblock, visited);
			}
		}
	}
	
	public void generateVcgForDominatorTree(Set<DominatorTree> dominatorTrees) throws IOException{
		vcgWriter.write("graph: { title: \"Dominator Tree\"\n"
				+ "layoutalgorithm: dfs\n"
				+ "manhattan_edges: yes\n"
				+ "smanhattan_edges: yes\n");
		for(DominatorTree domTree: dominatorTrees){
			HashSet<DominatorTreeNode> visited = new HashSet<DominatorTreeNode>();
			generateVcgForDominatorTreeUtil(domTree.getRoot(), visited);
		}
		vcgWriter.write("}");
		vcgWriter.flush();
		vcgWriter.close();
	}

	private void generateVcgForDominatorTreeUtil(DominatorTreeNode domTreeNode, HashSet<DominatorTreeNode> visited) throws IOException {
		if(visited.contains(domTreeNode)) return;
		visited.add(domTreeNode);
		DominatorTreeNode parentDomTreeNode = domTreeNode.getParent();
		Integer parentId = null;
		if(parentDomTreeNode != null){
			BasicBlock parentBlock = parentDomTreeNode.getBasicBlock();
			if(parentBlock != null){
				parentId = parentBlock.getBasicBlockId();
			}
		}
		Integer id = domTreeNode.getBasicBlock().getBasicBlockId();
		vcgWriter.write("node: {\n"
				+ "title: \""+id+"\"\n"
				+ "label: \""+id+"\n" + "parent: " + parentId + "\"}");
		for(DominatorTreeNode childDominatorTreeNode: domTreeNode.getChildren()){
			vcgWriter.write("edge: { sourcename: \""+id+"\"\n"
					+ "targetname: \""+childDominatorTreeNode.getBasicBlock().getBasicBlockId()+"\"\n"
							+ "color: blue\n"
							+ "}\n");
		}
		
		for(DominatorTreeNode childDominatorTreeNode: domTreeNode.getChildren()){
			generateVcgForDominatorTreeUtil(childDominatorTreeNode, visited);
			vcgWriter.flush();
		}
	}
	
	public void generateVcgForInterferenceGraph(InterferenceGraph ifg) throws IOException{
		vcgWriter.write("graph: { title: \"Interference Graph\"\n"
				+ "layoutalgorithm: dfs\n"
				+ "manhattan_edges: yes\n"
				+ "smanhattan_edges: yes\n");
		Map<Result, Set<Result>> adjacencyList = ifg.getAdjacencyList();
		//write nodes
		for(Entry<Result, Set<Result>> keyVal: adjacencyList.entrySet()){
			Result nodes = keyVal.getKey();
			vcgWriter.write("node: {\n"
					+ "title: \"" + nodes + "\"\n"
					+ "label: \"" + nodes + "\"\n}\n");
		}
		for(Entry<Result, Set<Result>> keyVal: adjacencyList.entrySet()){
			Result edge1 = keyVal.getKey();
			Set<Result> edges = keyVal.getValue();
			for(Result edge2: edges){
				vcgWriter.write("edge: { sourcename: \""+ edge1 +"\"\n"
						+ "targetname: \"" + edge2 +"\"\n"
								+ "color: blue\n"
								+ "}\n");
			}
			vcgWriter.flush();
		}
		vcgWriter.write("}");
		vcgWriter.flush();
		vcgWriter.close();
	}
}
