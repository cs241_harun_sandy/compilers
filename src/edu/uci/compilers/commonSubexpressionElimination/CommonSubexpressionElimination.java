package edu.uci.compilers.commonSubexpressionElimination;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.uci.compilers.dataTypes.BasicBlock;
import edu.uci.compilers.dataTypes.Instruction;
import edu.uci.compilers.dataTypes.Operation;
import edu.uci.compilers.dataTypes.Result;
import edu.uci.compilers.dataTypes.Result.Type;
import edu.uci.compilers.dominatorTree.DominatorTree;
import edu.uci.compilers.dominatorTree.DominatorTreeNode;

public class CommonSubexpressionElimination {
	private Set<DominatorTree> dominatorTrees;
	private boolean isCseDone;
	
	public boolean getIsCseDone(){
		return isCseDone;
	}
	public CommonSubexpressionElimination(){
		isCseDone = false;
	}
	public CommonSubexpressionElimination(Set<DominatorTree> domTree){
		this.dominatorTrees = domTree;
	}
	
	public void setDominatorTree(Set<DominatorTree> dominatorTree) {
		this.dominatorTrees = dominatorTree;
	}

	private void populateCseMap(Map<Operation, ArrayList<Instruction>> cseMap){
		for(Operation op: Operation.values()){
			ArrayList<Instruction> instructions = new ArrayList<Instruction>();
			cseMap.put(op, instructions);
		}
	}
	public void doCommonSubexpressionElimination(){
		isCseDone = false;
		Map<Operation, ArrayList<Instruction>> cseMap = new HashMap<Operation, ArrayList<Instruction>>();
		populateCseMap(cseMap);
		//now do a DFS over dom trees
		List<Instruction> propagateInstructions = new ArrayList<Instruction>();
		for(DominatorTree domTree: dominatorTrees){
			cseDfs(domTree.getRoot(), cseMap, propagateInstructions);
		}
		if(propagateInstructions.size() > 0)
			isCseDone = false;
		else
			isCseDone = true;
	}

	private void cseDfs(DominatorTreeNode domTreeNode, Map<Operation, ArrayList<Instruction>> cseMap, 
			List<Instruction> propagateInstructions) {
		//the DFS
		BasicBlock basicBlock = domTreeNode.getBasicBlock();
		//iterate over instructions
//		for(Instruction instr: basicBlock.getInstructions()){
		Iterator<Instruction> iter = basicBlock.getInstructions().iterator();
		int programCounter = -1;
		while(iter.hasNext()){
			programCounter ++;
			Instruction instr = iter.next();
			Operation op = instr.getOperation();
			
			List<Instruction> similarInstructions = cseMap.get(op);
			
			if(op == Operation.WRITE || op == Operation.READ || op == Operation.WRITENL) continue;
			if(op == Operation.DUMMY_KILL){
				similarInstructions.add(instr);
				continue;
			}
			if(op == Operation.LOAD){
				Result cseResult = new Result();
				boolean killed = findIfLoadKilled(domTreeNode, instr, cseResult, programCounter);
				if(!killed){
					instr.setOperation(Operation.PROPAGATE);
					instr.setOperand1(cseResult);
					propagateInstructions.add(instr);
				}
				continue;
			}
			boolean isCse = false;
			for(Instruction cseCandidateInstr: similarInstructions){
				if(cseCandidateInstr.equals(instr)){
					//its a common subexp! replace the instruction with "prop (cseCandidateInstr's Id)" we will propagate later
					int cseInstrId = cseCandidateInstr.getId();
					
					Result cseInstrResult = new Result();
					cseInstrResult.setType(Type.INSTRUCTION);
					cseInstrResult.setInstructionId(cseInstrId);
					
					instr.setOperation(Operation.PROPAGATE);
					instr.setOperand1(cseInstrResult);
					propagateInstructions.add(instr);
					
					isCse = true;
					break;
				}
			}
			if(!isCse){//then add it to the list!
				similarInstructions.add(instr);
			}
		}//now the current domtreenode is done
		//dfs over its children
		for(DominatorTreeNode childDomTreeNode: domTreeNode.getChildren()){
			Map<Operation, ArrayList<Instruction>> newCseMap = new HashMap<Operation, ArrayList<Instruction>>(cseMap);
			cseDfs(childDomTreeNode, newCseMap, propagateInstructions);
		}
	}
	private boolean findIfLoadKilled(DominatorTreeNode domTreeNode, Instruction instr, Result cseInstr, int index) {
		BasicBlock basicBlock = domTreeNode.getBasicBlock();
		List<Instruction> instructions = basicBlock.getInstructions();
		Result arrayLoaded = instr.getArrayVar();
		for(int i = index-1; i >= 0; i--){
			Instruction candidateInstr = instructions.get(i);
			if(candidateInstr.equals(instr)){
				cseInstr.setType(Type.INSTRUCTION);
				cseInstr.setInstructionId(candidateInstr.getId());
				return false;
			}
			Operation op = candidateInstr.getOperation();
			if(op == Operation.STORE || op == Operation.DUMMY_KILL){//now check if the load was killed
				Result arrayKilled = candidateInstr.getArrayVar();
				if(arrayLoaded.equals(arrayKilled)) return true;
			}
		}
		DominatorTreeNode parent = domTreeNode.getParent();
		if(parent != null){
			return bottomUpSearch(parent, instr, cseInstr);
		} else {//there was no other load previously
			return true;
		}
	}
	private boolean bottomUpSearch(DominatorTreeNode domTreeNode, Instruction instr, Result cseInstr) {
		BasicBlock basicBlock = domTreeNode.getBasicBlock();
		List<Instruction> instructions = basicBlock.getInstructions();
		Result arrayLoaded = instr.getArrayVar();
		for(int i = instructions.size()-1; i >= 0; i--){
			Instruction candidateInstr = instructions.get(i);
			if(candidateInstr.equals(instr)){
				cseInstr.setType(Type.INSTRUCTION);
				cseInstr.setInstructionId(candidateInstr.getId());
				return false;
			}
			Operation op = candidateInstr.getOperation();
			if(op == Operation.STORE || op == Operation.DUMMY_KILL){
				Result arrayKilled = candidateInstr.getArrayVar();
				if(arrayLoaded == arrayKilled) return true;
			}
		}
		DominatorTreeNode parent = domTreeNode.getParent();
		if(parent != null){
			return bottomUpSearch(parent, instr, cseInstr);
		} else{
			return true;
		}
	}
}
