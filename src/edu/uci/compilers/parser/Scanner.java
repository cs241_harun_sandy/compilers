package edu.uci.compilers.parser;

import java.io.IOException;
import java.util.ArrayList;

import edu.uci.compilers.dataTypes.Token;


public class Scanner {
	
	private char currentSym;
	private Reader reader;
	private boolean isEOF;
	private Token currentToken;
	private int number;
	private int id;
	private String currId;
	private ArrayList<String> identifiers;
	
	public Scanner(String fileName) throws IOException{
		identifiers = new ArrayList<String>();
		id=-1;
		isEOF=false;
		reader = new Reader(fileName);
		currentSym = reader.getSym();
		if(currentSym==255){
			isEOF=true;
		}
		getNextToken();
	}
	
	public int getCurrentLine(){
		return reader.getCurrentLine();
	}
	
	public String getCurrId() {
		return currId;
	}
	private void moveToNextSym() throws IOException{
		if(currentSym==255){
			isEOF=true;
			currentSym=255;
		} else {
			currentSym = reader.getSym();
		}
	}
	
	private void consumeLine() throws IOException{
		reader.consumeLine();
	}

	private void consumeCommentsAndWhiteSpaces() throws IOException{
		while(currentSym==' ' || currentSym=='\t' || currentSym=='#' || currentSym=='/'){
			if(currentSym=='/'){
				if(reader.peekSym()=='/'){ //go to next line iff next char is /
					consumeLine();
					moveToNextSym();
				} else break;
			} else if(currentSym=='#'){
				consumeLine();
				moveToNextSym();
			} else{
				moveToNextSym();
			}
		}
	}
	
	public Token getNextToken() throws IOException{ // updates sym with the current token
		consumeCommentsAndWhiteSpaces();
		if(currentSym==255){
			currentToken=Token.EOF;
		} else {
			char peek=reader.peekSym();
			currentToken=Token.INIT;
			switch(currentSym){
			case '*':
				currentToken = Token.TIMES;
				break;
			case '/':
				currentToken =  Token.DIV;
				break;
			case '+':
				currentToken = Token.PLUS;
				break;
			case '-':
				currentToken = Token.MINUS;
				break;
			case '=':
				if(peek=='='){
					moveToNextSym();
					currentToken = Token.EQL;
				} else {
					currentToken = Token.ERROR;
				}
				break;
			case '!':
				if(peek=='='){
					moveToNextSym();
					currentToken = Token.NEQ;
				} else {
					currentToken = Token.ERROR;
				}
				break;
			case '<':
				if(peek=='='){
					moveToNextSym();
					currentToken = Token.LEQ;
				} else if(peek=='-'){
					moveToNextSym();
					currentToken = Token.BECOMES;
				} else {
					currentToken = Token.LSS;
				}
				break;
			case '>':
				if(peek=='='){
					moveToNextSym();
					currentToken = Token.GEQ;
				} else {
					currentToken = Token.GTR;
				}
				break;
			case '.':
				currentToken = Token.PERIOD;
				break;
			case ',':
				currentToken = Token.COMMA;
				break;
			case '[':
				currentToken = Token.OPENBRACKET;
				break;
			case ']':
				currentToken = Token.CLOSEBRACKET;
				break;
			case ')':
				currentToken = Token.CLOSEPAREN;
				break;
			case '(':
				currentToken = Token.OPENPAREN;
				break;
			case ';':
				currentToken = Token.SEMI;
				break;
			case '}':
				currentToken = Token.END;
				break;
			case '{':
				currentToken = Token.BEGIN;
				break;
			}
			if(currentToken!=Token.INIT){
				moveToNextSym();
			} else if(currentToken==Token.INIT) {
				if(isChar(currentSym)){
					String tok="";
					tok+=currentSym;
					char peeked=reader.peekSym();
					moveToNextSym();
					while(isChar(currentSym) || isDigit(currentSym)){
						tok+=currentSym;
						moveToNextSym();
						if(peeked=='$') break;
						peeked=reader.peekSym();
					}
					if(tok.equals("then")){
						currentToken=Token.THEN;
					} else if(tok.equals("do")){
						currentToken=Token.DO;
					} else if(tok.equals("od")){
						currentToken=Token.OD;
					} else if(tok.equals("fi")){
						currentToken=Token.FI;
					} else if(tok.equals("else")){
						currentToken=Token.ELSE;
					} else if(tok.equals("let")){
						currentToken=Token.LET;
					} else if(tok.equals("call")){
						currentToken=Token.CALL;
					} else if(tok.equals("if")){
						currentToken=Token.IF;
					} else if(tok.equals("while")){
						currentToken=Token.WHILE;
					} else if(tok.equals("return")){
						currentToken=Token.RETURN;
					} else if(tok.equals("var")){
						currentToken=Token.VAR;
					} else if(tok.equals("array")){
						currentToken=Token.ARR;
					} else if(tok.equals("function")){
						currentToken=Token.FUNC;
					} else if(tok.equals("procedure")){
						currentToken=Token.PROC;
					} else if(tok.equals("main")){
						currentToken=Token.MAIN;
					} else {//this is an identifier
						int index=identifiers.indexOf(tok);
						currId=tok;
						if(index!=-1){//identifier already present in the list
							id=index;
						} else {
							identifiers.add(tok);
							id=identifiers.size()-1;
						}
						currentToken=Token.IDENT;
					}
				} else if(isDigit(currentSym)){//number
					int val=currentSym-'0';
					moveToNextSym();
					while(isDigit(currentSym)){
						val=val*10+(currentSym-'0');
						moveToNextSym();
					}
					this.number=val;
					currentToken=Token.NUMBER;
				} else {
					currentToken=Token.ERROR;
				}
			}
		}
		return currentToken;
	}

	private boolean isDigit(char c) {
		return c>='0' && c<='9';
	}

	private boolean isChar(char c) {
		return (c>='a' && c<='z') || (c>='A' && c<='Z');
	}

	public Token getCurrentToken() {
		return currentToken;
	}

	public int getNumber() {
		return number;
	}

//	Driver
//	public static void main(String[] args) throws IOException{
//		Scanner scan = new Scanner("file.inp");
//		scan.getSym();
//	}
}

