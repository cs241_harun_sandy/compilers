package edu.uci.compilers.parser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import static edu.uci.compilers.dataTypes.ErrorMessage.*;
import edu.uci.compilers.cfg.ControlFlowGraph;
import edu.uci.compilers.cfg.IntermediateCodeGenerator;
import edu.uci.compilers.dataTypes.BasicBlock;
import edu.uci.compilers.dataTypes.BasicBlock.BasicBlockType;
import edu.uci.compilers.dataTypes.GlobalVariableManager;
import edu.uci.compilers.dataTypes.Instruction;
import edu.uci.compilers.dataTypes.PhiFunctionManager;
import edu.uci.compilers.dataTypes.Function;
import edu.uci.compilers.dataTypes.Result;
import edu.uci.compilers.dataTypes.Result.Type;
import edu.uci.compilers.dataTypes.Token;
import edu.uci.compilers.dataTypes.Utils;

public class Parser {
	
	private Scanner scanner;
	private Token currentToken;
	private ControlFlowGraph cfg;
	private boolean parseSuccess;
	private IntermediateCodeGenerator icg;
	private GlobalVariableManager gvm;
	private PhiFunctionManager phiManager;
	
//	public void setCfg(ControlFlowGraph cfg) {
//		this.cfg = cfg;
//	}
//	
//	public void setIcg(IntermediateCodeGenerator icg) {
//		this.icg = icg;
//	}

	public Parser(String fileName) throws IOException{
		scanner = new Scanner(fileName);
		currentToken = scanner.getCurrentToken();
		parseSuccess=true;
		gvm = GlobalVariableManager.getInstance();
		icg = new IntermediateCodeGenerator();
		cfg = new ControlFlowGraph();
		phiManager = new PhiFunctionManager(icg);
	}
	
	public ControlFlowGraph getCfg() {
		return cfg;
	}
	protected void moveToNextToken() throws IOException{
		currentToken = scanner.getNextToken();
	}
	
	public void declareVariable(String identifier, List<Integer> dimensions, Function function, BasicBlock basicBlock){
		
		if(function == null){
			if(dimensions ==null){ //its a VAR
				gvm.addVariable(identifier);
				icg.generateICToInitializeVar(identifier, function, basicBlock);
			} else {
				gvm.addArrayVariable(identifier, dimensions);
			}
		}else{
			if(dimensions == null){
				function.addLocalVariable(identifier);
				icg.generateICToInitializeVar(identifier, function, basicBlock);
			}else{
				function.addLocalArrayVariable(identifier, dimensions);
			}
		}
		
	}
	

	private void varDecl(BasicBlock basicBlock, Function function) throws IOException{
		ArrayList<Integer> dimensions = typeDecl(); //returns null if a VAR or the dimension list if an ARR
		if(currentToken==Token.IDENT){
			declareVariable(getIdentifier(), dimensions, function, basicBlock);			
			moveToNextToken();
			while(currentToken==Token.COMMA){
				moveToNextToken();
				if(currentToken==Token.IDENT){
					declareVariable(getIdentifier(), dimensions, function, basicBlock);
					moveToNextToken();
				} else error(IDENTIFIER_EXPECTED);
			}
			if(currentToken==Token.SEMI){
				moveToNextToken();
			} else error(SEMI_EXPECTED);
		} else error(IDENTIFIER_EXPECTED);
	}
	
	private int number() throws IOException{
		if(currentToken==Token.NUMBER){
			int val = scanner.getNumber();
			moveToNextToken();
			return val;
		} else error(NUMBER_EXPECTED);
		return 0;
	}
	
	private ArrayList<Integer> typeDecl() throws IOException {
		ArrayList<Integer> dimensions=null;
		if(currentToken==Token.VAR){
			moveToNextToken();
		} else if(currentToken==Token.ARR){
			moveToNextToken();
			dimensions = new ArrayList<Integer>();
			if(currentToken==Token.OPENBRACKET){
				moveToNextToken();
				int dimension=number();
				dimensions.add(dimension);
				if(currentToken==Token.CLOSEBRACKET){
					moveToNextToken();
					while(currentToken==Token.OPENBRACKET){
						moveToNextToken();
						int inDimension = number();
						dimensions.add(inDimension);
						if(currentToken==Token.CLOSEBRACKET){
							moveToNextToken();
						} else error(CLOSEBRACKET_NOT_FOUND);
					}
				} else error(CLOSEBRACKET_NOT_FOUND);
			} else error(OPENBRACKET_NOT_FOUND);
		}
		return dimensions;
	}

	private void funcDecl() throws IOException{
		Function function = null;
		if(currentToken==Token.FUNC || currentToken==Token.PROC){
			moveToNextToken();
			if(currentToken==Token.IDENT){
				String functionName = getIdentifier();
				function = new Function(functionName);
				gvm.addFunction(functionName, function);
				moveToNextToken();
				formalParam(function);
				if(currentToken==Token.SEMI){
					moveToNextToken();
					funcBody(function);
					if(currentToken==Token.SEMI){
						moveToNextToken();
					} else error(SEMI_EXPECTED);
				} else error(SEMI_EXPECTED);
			} else error(IDENTIFIER_EXPECTED);
		} else error(FUNCTION_OR_PROCEDURE_EXPECTED);
	}
	
	private void funcBody(Function function) throws IOException {
		while(currentToken==Token.VAR || currentToken==Token.ARR){
			varDecl(function.getStartBlock(), function);
		}
		if(currentToken==Token.BEGIN){
			moveToNextToken();
			BasicBlock finalBlock = null;
			if(currentToken==Token.LET || currentToken==Token.CALL || currentToken==Token.IF
					|| currentToken==Token.WHILE || currentToken==Token.RETURN){
				finalBlock = statSequence(function.getStartBlock(), function, null);
			} else error("Statement Expected");
			if(currentToken==Token.END){
				moveToNextToken();
				function.setEndBlock(finalBlock);
			}
			else error(END_NOT_FOUND);
		} else error(BEGIN_NOT_FOUND);
	}

	private void formalParam(Function function) throws IOException { //dont return anything. if not formalParam just exit
		if(currentToken==Token.OPENPAREN){
			moveToNextToken();
			if(currentToken==Token.IDENT){
				moveToNextToken();
				String varName = getIdentifier();
				//Add the function parameter to both the parameter list and localVariable map
				function.addLocalVariable(varName);
				function.addParameter(varName);
				while(currentToken==Token.COMMA){
					moveToNextToken();
					varName = getIdentifier();
					function.addLocalVariable(varName);
					function.addParameter(varName);
					if(currentToken==Token.IDENT)
						moveToNextToken();
					else 
						error(IDENTIFIER_EXPECTED);
				}
			}
			if(currentToken==Token.CLOSEPAREN){
				moveToNextToken();
			} else error(CLOSEPAREN_NOT_FOUND);
		}
	}

	private BasicBlock statSequence(BasicBlock basicBlock, Function function, List<Instruction> storeKills) throws IOException{ //consume stats or else dont return error
		basicBlock=statement(basicBlock, function, storeKills);
		while(currentToken==Token.SEMI){
			moveToNextToken();
			basicBlock=statement(basicBlock, function, storeKills);
		}
		return basicBlock;
	}
	private BasicBlock statement(BasicBlock basicBlock, Function function, List<Instruction> storeKills) throws IOException {
		BasicBlock returnBlock=null;
		if(currentToken==Token.LET){
			assignment(basicBlock, function, storeKills);
			returnBlock=basicBlock;
		} else if(currentToken==Token.CALL){
			funcCall(basicBlock);
			returnBlock = basicBlock;
		} else if(currentToken==Token.IF){
			returnBlock=ifStatement(basicBlock, function, storeKills);
		} else if(currentToken==Token.WHILE){
			returnBlock = whileStatement(basicBlock, function, storeKills);
		} else if(currentToken==Token.RETURN){
			returnStatement(basicBlock, function);
			returnBlock = basicBlock;
		} else error("Syntax Error!");
		
		return returnBlock;
	}

	private void returnStatement(BasicBlock basicBlock, Function function) throws IOException {

		if(currentToken==Token.RETURN){
			moveToNextToken();
			if(currentToken != Token.END){
				Result result = expression(basicBlock, function);//return true if expression else not
				icg.generateICForReturnStatemnt(basicBlock, result);
			}
		}
	}

	private Result expression(BasicBlock basicBlock, Function function) throws IOException {

		Result resultLeft=term(basicBlock, function);
		Result resultRight=null;
		while(currentToken==Token.PLUS || currentToken==Token.MINUS){
			Token tokenOp=currentToken;
			moveToNextToken();
			resultRight=term(basicBlock, function);
			resultLeft=icg.generateICForExpression(basicBlock, resultLeft, resultRight, tokenOp);
		}
		return resultLeft;
	}

	private Result term(BasicBlock basicBlock, Function function) throws IOException {

		Result termLeft=factor(basicBlock, function);
		Result termRight=null;
		while(currentToken==Token.TIMES || currentToken==Token.DIV){
			Token tokenOp = currentToken;
			moveToNextToken();
			termRight=factor(basicBlock, function);
			termLeft = icg.generateICForExpression(basicBlock, termLeft, termRight, tokenOp);
		}
		return termLeft;
	}

	private Result factor(BasicBlock basicBlock, Function function) throws IOException {
		Result result=null;
		if(currentToken==Token.IDENT){
			boolean fromAssignment = false;
			Result arrayRes = new Result();
			result=designator(basicBlock, function, fromAssignment, arrayRes);
		} else if(currentToken==Token.NUMBER){
			int num=number();
			result=new Result();
			result.setType(Type.CONSTANT);
			result.setValue(num);
		} else if(currentToken==Token.OPENPAREN){
			moveToNextToken();
			result=expression(basicBlock, function);
			if(currentToken==Token.CLOSEPAREN){
				moveToNextToken();
			} else error(CLOSEPAREN_NOT_FOUND);
		} else if(currentToken==Token.CALL){ //to check if its fn call. dont call next because funcCall will take care
			result = funcCall(basicBlock);
		} else error("Not a factor");
		return result;
	}

	private BasicBlock whileStatement(BasicBlock basicBlock, Function function, List<Instruction> storeKills) throws IOException {
		BasicBlock fallThroughBlock = null;
		if(currentToken==Token.WHILE){
			moveToNextToken();
			BasicBlock whileBlock = new BasicBlock(BasicBlockType.WHILE);
			basicBlock.addBasicBlockChildren(whileBlock);
//			whileBlock.addBasicBlockParent(basicBlock);
			
			fallThroughBlock = new BasicBlock(BasicBlockType.NORMAL);
//			fallThroughBlock.addBasicBlockParent(whileBlock);
			
			BasicBlock whileBodyBlock = new BasicBlock(BasicBlockType.WHILEBODY);
			//While block and while body block point to each other. It is a circle
			whileBlock.addBasicBlockChildren(whileBodyBlock);
			//whileBodyBlock.addBasicBlockChildren(whileBlock);
//			whileBlock.addBasicBlockParent(whileBodyBlock);
//			whileBodyBlock.addBasicBlockParent(whileBlock);
			
			Result fixup = relation(whileBlock, function);
			icg.fixUpLocation(fixup, fallThroughBlock);
			if(currentToken==Token.DO){
				moveToNextToken();
				BasicBlock whileBodyEndBlock = new BasicBlock();
				whileBodyEndBlock = statSequence(whileBodyBlock, function, storeKills);
				icg.insertFallThroughBranch(whileBodyEndBlock, whileBlock);
				whileBodyEndBlock.addBasicBlockChildren(whileBlock);
				phiManager.insertPhiFunctionsForWhile(whileBlock, basicBlock, whileBodyBlock, whileBodyEndBlock);
				if(currentToken==Token.OD){
					moveToNextToken();

					whileBlock.addBasicBlockChildren(fallThroughBlock);
				}
				else error(OD_EXPECTED);
			} else error(DO_EXPECTED);
		}
		return fallThroughBlock;
	}

	private Result relation(BasicBlock basicBlock, Function function) throws IOException {

		Result leftExp=expression(basicBlock, function);
		Result condition=relOp();
		Result rightExp=expression(basicBlock, function);
		Result fixup = icg.generateICforRelation(basicBlock, leftExp, condition, rightExp);
		return fixup;
//		icg.generateICForRelation(basicBlock, leftExp, condition, rightExp);
	}

	private Result relOp() throws IOException {
		Result condition=null;
		if(currentToken==Token.EQL || currentToken==Token.NEQ || currentToken==Token.LSS
				|| currentToken==Token.LEQ || currentToken==Token.GTR || currentToken==Token.GEQ){
			condition=new Result();
			condition.setType(Type.CONDITION);
			condition.setRelationOperator(currentToken);
			moveToNextToken();
		} else error(RELATIONAL_OPERATOR_NOT_FOUND);
		return condition;
	}

	private BasicBlock ifStatement(BasicBlock basicBlock, Function function, List<Instruction> storeKills) throws IOException {//should return a new basic block i.e., the join block
		//every basic block must maintain a list of variables that changed so that we can produce phi functions
		BasicBlock joinBlock = null;
		BasicBlock elseBlock = null;
				
		if(currentToken==Token.IF){

			moveToNextToken();
		
			BasicBlock ifBlock = new BasicBlock(BasicBlockType.IF);
			
			basicBlock.addBasicBlockChildren(ifBlock);
//			ifBlock.addBasicBlockParent(basicBlock);
			
			joinBlock = new BasicBlock();
			
			Result fixup = relation(basicBlock, function);
			
			List<Instruction> ifPathStoreKill = new ArrayList<Instruction>();
			List<Instruction> elsePathStoreKill = new ArrayList<Instruction>();
			
			if(currentToken == Token.THEN){
				moveToNextToken();
				joinBlock.setBottomUpMergerBasicBlock(basicBlock);//only needed for if - else join!
				ifBlock=statSequence(ifBlock, function, ifPathStoreKill);
				if(currentToken==Token.ELSE){// if-else
					moveToNextToken();
					
					elseBlock = new BasicBlock(BasicBlockType.ELSE);
					basicBlock.addBasicBlockChildren(elseBlock);
//					elseBlock.addBasicBlockParent(basicBlock);
					
					icg.fixUpLocation(fixup, elseBlock);
					
					elseBlock = statSequence(elseBlock, function, elsePathStoreKill);
					
//					joinBlock.addBasicBlockParent(ifBlock);
//					joinBlock.addBasicBlockParent(elseBlock);
					
					ifBlock.addBasicBlockChildren(joinBlock);
					elseBlock.addBasicBlockChildren(joinBlock);
					
					icg.insertDummyKills(joinBlock, ifPathStoreKill, elsePathStoreKill);
					
					joinBlock.setType(BasicBlockType.IF_ELSE_JOIN);
					phiManager.createPhiFunctionsAndLocalSsaMap(joinBlock, ifBlock, elseBlock);
					
				} else { //just an if
//					joinBlock.addBasicBlockParent(ifBlock);
					ifBlock.addBasicBlockChildren(joinBlock);
					basicBlock.addBasicBlockChildren(joinBlock);
					icg.fixUpLocation(fixup, joinBlock);
					joinBlock.setType(BasicBlockType.IF_JOIN);
					
					icg.insertDummyKills(joinBlock, ifPathStoreKill, elsePathStoreKill);
					
					phiManager.createPhiFunctionsAndLocalSsaMap(joinBlock, ifBlock, basicBlock);
				}
				if(currentToken==Token.FI){
					moveToNextToken();
					if(storeKills != null){
						storeKills.addAll(ifPathStoreKill);
						storeKills.addAll(elsePathStoreKill);
					}
					
					icg.insertFallThroughBranch(ifBlock, joinBlock);
				} else error(FI_EXPECTED);
			} else error(THEN_EXPECTED);
		}
		return joinBlock;
		//return joinBlock; <- change signature of this method and uncomment this line
	}

	private Result funcCall(BasicBlock basicBlock) throws IOException {
		Result result=null;
		if(currentToken==Token.CALL){
			moveToNextToken();
			if(currentToken==Token.IDENT){
				String functionName = getIdentifier();
				if(functionName.equals("InputNum") || functionName.equals("OutputNum") || 
						functionName.equals("OutputNewLine")){
					moveToNextToken();
					Result param = getParamForSpecialFunction(basicBlock);
					result = icg.generateICForSpecialFunctioncall(basicBlock, functionName, param);
				}
				else {
					Function function = gvm.getFunction(functionName);
					basicBlock.addFunctionsCalled(function);
					moveToNextToken();
					if(currentToken==Token.OPENPAREN){
						moveToNextToken();
						if(currentToken!=Token.CLOSEPAREN){
							List<Result> parameters = new ArrayList<Result>();
							Result param = expression(basicBlock, function);
							parameters.add(param);
//							icg.generateICForFunctionParam(basicBlock, param);
							while(currentToken==Token.COMMA){
								moveToNextToken();
								param = expression(basicBlock, function);
								parameters.add(param);
//								icg.generateICForFunctionParam(basicBlock, param);
							}
							icg.generateICForFunctionParams(basicBlock, parameters);
	//						result = icg.generateICForFunctionCall(basicBlock, function);
						}
						if(currentToken==Token.CLOSEPAREN){
							moveToNextToken();
							result = icg.generateICForFunctionCall(basicBlock, function);
						} else error(CLOSEPAREN_NOT_FOUND);
					}
				}
			} else error(IDENTIFIER_EXPECTED);
		}
		return result;
	}

	private Result getParamForSpecialFunction(BasicBlock basicBlock) throws IOException {
		Result param = null;
		if(currentToken == Token.OPENPAREN){
			moveToNextToken();
			if(currentToken == Token.IDENT){//can only be a variable or constant.
				String identifier = getIdentifier();
				
				moveToNextToken();
				Integer ssaVersion = basicBlock.getSsaMapOfVaribale(identifier);
				
				param = new Result();
				param.setIdentifierName(identifier);
				param.setSsaVersion(ssaVersion);
				param.setType(Type.VARIABLE);
			} else if(currentToken == Token.NUMBER){
				int num = number();
//				moveToNextToken();
				param = new Result();
				param.setType(Type.CONSTANT);
				param.setValue(num);
			}
			if(currentToken == Token.CLOSEPAREN){//no parameter
				moveToNextToken();
			}
			else error(CLOSEPAREN_NOT_FOUND);
		}
		return param;
	}

	private Result assignment(BasicBlock basicBlock, Function function, List<Instruction> storeKills) throws IOException {
		Result lhs, rhs;
		lhs=rhs=null;
		if(currentToken==Token.LET){
			moveToNextToken();
			boolean calledThroughAssignment = true;
			Result lhsArrayRes = new Result();
			lhs=designator(basicBlock, function, calledThroughAssignment, lhsArrayRes);
			if(lhs.getType() == Type.CONSTANT) error(LVALUE_REQUIRED);
			
			if(currentToken==Token.BECOMES){
				moveToNextToken();
				rhs=expression(basicBlock, function);
				icg.generateICForAssignment(basicBlock, rhs, lhs, function, lhsArrayRes, storeKills);
			} else error(BECOMES_EXPECTED);
		}
		return lhs;
	}

	private Result designator(BasicBlock basicBlock, Function function, boolean fromAssignment, Result arrayRes) throws IOException {//returns a VAR result for var and Instruction result for array
		Result result=null;//TODO arr[2][3][4] = var has an unnecessary load. get rid of it
		if(currentToken==Token.IDENT){
			String identifier=getIdentifier();
			result=new Result();
			moveToNextToken();
			int dimensionCount=0;
			Result prevDimExp=null, currentDimExp=null;
			int currentDimension=1;
			List<Integer> dimensions = null;
			if(function == null)
				dimensions = icg.getArrayDimensions(identifier);
			else{
				Map<String, List<Integer>> dims = function.getLocalArrayVariablesVsDims();
				if(dims!=null){
					dimensions = dims.get(identifier);
				}
			}
			if(dimensions == null){//to handle function parameters which are global array variables
				dimensions = icg.getArrayDimensions(identifier);
			}
			ArrayList<Result> dimensionExps = new ArrayList<Result>();
			while(currentToken==Token.OPENBRACKET){ //array variable
				moveToNextToken();
				currentDimExp = expression(basicBlock, function);//for array variables
				dimensionExps.add(currentDimExp);
				if(currentToken==Token.CLOSEBRACKET){
					moveToNextToken();
				} else error(CLOSEBRACKET_NOT_FOUND);
				dimensionCount+=1;
			}
			if(dimensionCount!=0){
				
				Result addResult = icg.generateICForArrayDimensions(basicBlock, dimensionExps, dimensions);
				
				Result loadResult = icg.generateICForArrayEnds(basicBlock, identifier, addResult, fromAssignment);
				
//				Result loadedAddress = new Result();
//				loadedAddress.setType(Type.INSTRUCTION);
//				loadedAddress.setInstructionId(loadInstr.getId());
				arrayRes.setType(Type.ARRAYVAR);
				arrayRes.setIdentifierName(identifier);
				
				return loadResult;
			} else {
				result.setType(Result.Type.VARIABLE);
				result.setIdentifierName(identifier);
				if(function==null){
					result.setSsaVersion(basicBlock.getSsaMapOfVaribale(identifier));
//					result.setSsaVersion(gvm.getLatestSSAVersion(identifier));
				} else {
					Integer latestSsa = function.getLatestSSA(identifier, basicBlock);
					if(latestSsa == null){//probably a global variable
						latestSsa = gvm.getLatestSSAVersion(identifier);
						if(latestSsa == null){//error variable not defined
							error(VARIABLE_NOT_DEFINED);
						}
					}
					result.setSsaVersion(latestSsa);
				}
			}
		} else error(IDENTIFIER_EXPECTED);
		return result;
	}

//	private int getSSA(String identifier, Function function) {
//		if(function == null){
//			return gvm.getLatestSSAVersion(identifier);
//		}else{
//			return function.getLatestSSA(identifier);
//		}
//	}

	private String getIdentifier() {
		String id = scanner.getCurrId();
		return id;
	}
	
	public void computation() throws IOException{
		if(currentToken==Token.MAIN){
			moveToNextToken();
			BasicBlock basicBlock=cfg.getStartBlock();
			while(currentToken==Token.VAR || currentToken==Token.ARR){
				varDecl(basicBlock, null);
			}
			Map<String, Integer> copyMap = Utils.deepCopySsaVersionMap(gvm.getVariablesVsSSAVersion());
			basicBlock.setSsaMap(copyMap);
			while(currentToken==Token.FUNC || currentToken==Token.PROC){
				funcDecl();
			}
			if(currentToken==Token.BEGIN){
				moveToNextToken();
				basicBlock=statSequence(basicBlock, null, null);
				if(currentToken==Token.END){
					moveToNextToken();
					if(currentToken==Token.PERIOD){
						moveToNextToken();
						icg.insertEnd(basicBlock, cfg);
						cfg.printCFG();
						cfg.populateParents();
					} else error(PERIOD_NOT_FOUND);
				} else error(END_NOT_FOUND);
			} else error(BEGIN_NOT_FOUND);
		} else error(MAIN_NOT_FOUND);
	}

	private void error(String errorMsg){
		System.out.println("Syntax error: "+errorMsg+" at line number "+scanner.getCurrentLine());
		parseSuccess=false;
	}
	
	public boolean getParseStatus(){
		return parseSuccess;
	}
	
	public Token getToken(){
		return currentToken;
	}
}
