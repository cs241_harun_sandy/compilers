package edu.uci.compilers.parser;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Reader {
	
	private BufferedReader reader;
	private char sym;
	private String line;
	private int lineNumber;
	private int lineLength;
	private int symNumber;
	private boolean isEOF;
	
	public int getCurrentLine(){
		return lineNumber;
	}
	public Reader(String fileName) {
		
		lineNumber=0; // 0 means that we havent read file yet
		symNumber=0;
		lineLength=0;
		line="";
		isEOF=false;
		sym='$';//End
		try {
			reader=new BufferedReader(new FileReader(fileName));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	private void closeFile() throws IOException{
		reader.close();
	}
	
	public char peekSym(){ // solely to find if a line is a comment
						   // to be used only when currchar is '/'
		if(isEOF){
			return 255;
		}
		if(lineNumber==0 || symNumber >= lineLength){
			return '$';//to denote new line
		} else {
			return line.charAt(symNumber);
		}
	}
	
	public void consumeLine() throws IOException{//called when a comment is encountered
		line=reader.readLine();
		if(line==null){
			isEOF=true;
			closeFile();
		}
		lineNumber+=1;
		lineLength=line.length();
		symNumber=0;
	}
	
	public char getSym() throws IOException{
		if(isEOF) return 255;
		if(lineNumber==0 || symNumber >= lineLength){//we havent read anything or no more character in the line
			line=reader.readLine();
			while(line!=null && line.trim().isEmpty()){
				line=reader.readLine();
			}
			if(line==null){
				isEOF=true;
				closeFile();
				return 255;
			}
			lineNumber+=1;
			lineLength=line.length();
			symNumber=0;
		}
		sym=line.charAt(symNumber);
		symNumber+=1;
		return sym;
	}
}
