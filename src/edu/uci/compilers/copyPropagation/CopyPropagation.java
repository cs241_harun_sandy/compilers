package edu.uci.compilers.copyPropagation;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import edu.uci.compilers.dataTypes.BasicBlock;
import edu.uci.compilers.dataTypes.Instruction;
import edu.uci.compilers.dataTypes.Operation;
import edu.uci.compilers.dataTypes.Result;
import edu.uci.compilers.dataTypes.Result.Type;
import edu.uci.compilers.dominatorTree.DominatorTree;
import edu.uci.compilers.dominatorTree.DominatorTreeNode;

public class CopyPropagation {//TODO - deadcode elimination
	private Set<DominatorTree> dominatorTrees;
//	private Map<Result, Result> 
	
	public CopyPropagation(){
		this.dominatorTrees = null;
	}
	
	public CopyPropagation(Set<DominatorTree> dominatorTree){
		this.dominatorTrees = dominatorTree;
	}
	
	public void setDominatorTree(Set<DominatorTree> dominatorTree) {
		this.dominatorTrees = dominatorTree;
	}
	
	public void makeAllCopyPropagations(){
		for(DominatorTree domTree: dominatorTrees){
			doCopyPropogation(domTree.getRoot());
		}
	}
	
	private void doCopyPropogation(DominatorTreeNode domTreeRoot){
		//dfs in dominator tree to do copy propagation
		HashMap<Result, Result> copyPropagatorMap = new HashMap<Result, Result>();
		HashSet<Integer> visitedBasicBlockIds = new HashSet<Integer>();
		copyPropagator(domTreeRoot, copyPropagatorMap, visitedBasicBlockIds);
	}

	private void copyPropagator(DominatorTreeNode dominatorTreeNode, HashMap<Result, Result> copyPropagatorMap,
			HashSet<Integer> visitedBasicBlockIds) {
		// basically a DFS over the dominator tree to do copy propagation
		BasicBlock basicBlock = dominatorTreeNode.getBasicBlock();
		visitedBasicBlockIds.add(basicBlock.getBasicBlockId());
		//now loop through the instructions of the basic block
		Iterator<Instruction> iter = basicBlock.getInstructions().iterator();
		while(iter.hasNext()){
			Instruction instr = iter.next();
			Operation operation = instr.getOperation();
			if(operation == Operation.MOVE){//have to delete move instructions
				iter.remove();
				
				Result operand1 = instr.getOperand1();//can be a var, const, instr
				Result operand2 = instr.getOperand2();//a variable or register, not a constant (lvalue reqd)

				Result propagatedCopy = propagateCopies(copyPropagatorMap, operand1);//handle transitivities (a-> b, b -> c) then c should be propagated to a
				if(propagatedCopy == null){//first time inserting to hashmap 
					propagatedCopy = operand1;
				}
				copyPropagatorMap.put(operand2, propagatedCopy);
			}
			else if(operation == Operation.PROPAGATE){
				iter.remove();
				Result operand1 = instr.getOperand1();
				
				Result instrToPropagate = new Result();
				instrToPropagate.setType(Type.INSTRUCTION);
				instrToPropagate.setInstructionId(instr.getId());
				
				Result propagatedCopy = propagateCopies(copyPropagatorMap, operand1);
				if(propagatedCopy == null){
					propagatedCopy = operand1;
				}
				copyPropagatorMap.put(instrToPropagate, propagatedCopy);
								
			} else {//for other instructions we need to enforce the propagations
				Result operand1 = instr.getOperand1();
				Result operand2 = instr.getOperand2();
				Result operand3 = instr.getOperand3();
				
				if(operand1 != null){
					Result propagatedResult = copyPropagatorMap.get(operand1);
					if(propagatedResult != null){//this variable was copy propagated
						operand1 = propagatedResult;
					}
				}
				if(operand2 != null){
					Result propagatedResult = copyPropagatorMap.get(operand2);
					if(propagatedResult != null){
						operand2 = propagatedResult;
					}
				}
				if(operand3 != null){
					Result propagatedResult = copyPropagatorMap.get(operand3);
					if(propagatedResult != null){
						operand3 = propagatedResult;
					}
				}
				//at this point all the operands would have been updated;
				instr.setOperand1(operand1);
				instr.setOperand2(operand2);
				instr.setOperand3(operand3);
			}
		}
		HashMap<Result, Result> cpMap = new HashMap<Result, Result>(copyPropagatorMap);
		//now processed all instructions in the basic block, do a dfs on the dominator tree and copy the current copyPropagatorMap to it
		for(DominatorTreeNode childDominatorTreeNode: dominatorTreeNode.getChildren()){
//			cpMap = new HashMap<Result, Result>(copyPropagatorMap);
			copyPropagator(childDominatorTreeNode, cpMap, visitedBasicBlockIds);
		}
		//now to update the phi functions
		BasicBlock dominatorTreeNodeBasicBlock = dominatorTreeNode.getBasicBlock();
		for(BasicBlock childBasicBlock: dominatorTreeNodeBasicBlock.getChildren()){
			if(visitedBasicBlockIds.contains(childBasicBlock.getBasicBlockId())){//then we need to update phi functions
				updatePhiFunctionsForCopyPropagation(childBasicBlock, cpMap);
			}
		}
	}

	private void updatePhiFunctionsForCopyPropagation(BasicBlock childBasicBlock, HashMap<Result, Result> cpMap) {
		if(cpMap == null) return;
		List<Instruction> instructions = childBasicBlock.getInstructions();
		for(Instruction instr: instructions){
			Operation op = instr.getOperation();
			if(op == Operation.PHI){
				Result operand2 = instr.getOperand2();
				Result operand3 = instr.getOperand3();
				
				Result propagatedOperand2 = cpMap.get(operand2);
				Result propagatedOperand3 = cpMap.get(operand3);
				
				if(propagatedOperand2 == null) propagatedOperand2 = operand2;
				if(propagatedOperand3 == null) propagatedOperand3 = operand3;
				
				instr.setOperand2(propagatedOperand2);
				instr.setOperand3(propagatedOperand3);
			} else break; //as phi's appear only in the beginning
		}
	}

	private Result propagateCopies(HashMap<Result, Result> copyPropagatorMap, Result operand1) {
		Result result = null;
		if(!copyPropagatorMap.containsKey(operand1)){
			return result;
		}
		result = propagateCopies(copyPropagatorMap, copyPropagatorMap.get(operand1));
		if(result == null){
			result = copyPropagatorMap.get(operand1);
		}
		return result;
	}
}

