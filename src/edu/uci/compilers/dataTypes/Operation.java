package edu.uci.compilers.dataTypes;

public enum Operation {
	NEG("neg"),
	ADD("add"),
	SUB("sub"),
	MUL("mul"),
	DIV("div"),
	CMP("cmp"),
	
	ADDA("adda"),
	LOAD("load"),
	STORE("store"),
	MOVE("move"),
	PHI("phi"),
	END("end"),
	BRA("bra"),
	BNE("bne"),
	BEQ("beq"),
	BLE("ble"),
	BLT("blt"),
	BGE("bge"),
	BGT("bgt"),
	
	READ("read"),
	WRITE("write"),
	WRITENL("writeNL"), 
	
	RET("ret"),
	PARAM("param"), 
	CALL("call"),
	PROPAGATE("prop"),//used in CSE to denote a backward pointer to already existing instruction
	DUMMY_KILL("dummy_store");
	
	private String name;
	private Operation(String name){
		this.name = name;
	}
	public String getName(){
		return this.name;
	}
	
}
