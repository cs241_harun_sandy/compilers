package edu.uci.compilers.dataTypes;

public class Result {
	public enum Type{
		CONSTANT, VARIABLE, REGISTER, CONDITION, INSTRUCTION, BRANCHINSTR,
		PARAMETER_COUNT, FUNCTION,
		ARRAYVAR, FRAMEPOINTER, BASEADDRESS, DUMMY;
	}
	private Type type;
	private int value; //if the type is constant
	private int address; //if the type is variable
	private int ssaVersion; //ssa version if type is variable
	private int registerNumber; //if the type is register or condition
	private Token relationOperator; // if the type is condition
	private int fixupLocation; // if the type is condition, its the instruction ID
	private String identifierName; // if type is variable
	private int instructionId; // if type is instruction
	private int targetBasicBlock; // if the instruction is a branch instruction
	private int functionBasicBlock; //if type is FUNCTION
	private int parameterCount; // if type is PARAMETER_COUNT

	@Override
	public boolean equals(Object obj){
		Result result = (Result) obj;
		boolean eq = (this.type == result.getType());
		eq = eq & (this.value == result.getValue()) & (this.address == result.getAddress()) & (this.ssaVersion == result.getSsaVersion())
				& (this.registerNumber == result.getRegisterNumber()) & (this.fixupLocation == result.getFixupLocation())
				& (this.instructionId == result.getInstructionId()) & (this.targetBasicBlock == result.getTargetBasicBlock())
				& (this.functionBasicBlock == result.getFunctionBasicBlock()) & (this.parameterCount == result.getParameterCount());
		eq = eq & (this.identifierName.equals(result.getIdentifierName())) & (this.relationOperator == result.getRelationOperator());
		return eq;
	}
	
	@Override
	public int hashCode(){
		return 13 * (this.value + this.address + this.ssaVersion + this.registerNumber + this.fixupLocation +
				this.instructionId + this.targetBasicBlock + this.functionBasicBlock + this.parameterCount) + identifierName.length();
	}
	
	public Result(){//initialize
		this.type = Type.DUMMY;
		this.value = this.address = this.ssaVersion = this.registerNumber = this.fixupLocation 
				= this.instructionId = this.targetBasicBlock = this.functionBasicBlock
				= this.parameterCount = -1;
		this.relationOperator = Token.INIT;
		this.identifierName = "";
		
	}
	public Result(Result key) {
		this.type = key.getType();
		this.value = key.getValue();
		this.address = key.getAddress();
		this.ssaVersion = key.getSsaVersion();
		this.registerNumber = key.getRegisterNumber();
		this.relationOperator = key.getRelationOperator();
		this.fixupLocation = key.getFixupLocation();
		this.identifierName = key.getIdentifierName();
		this.instructionId = key.getInstructionId();
		this.targetBasicBlock = key.getTargetBasicBlock();
		this.functionBasicBlock = key.getFunctionBasicBlock(); 
		this.parameterCount = key.getParameterCount();
	}

	public int getTargetBasicBlock() {
		return targetBasicBlock;
	}
	public void setTargetBasicBlock(int targetBasicBlock) {
		this.targetBasicBlock = targetBasicBlock;
	}
	public int getInstructionId() {
		return instructionId;
	}
	public void setSsaVersion(int ssaVersion) {
		this.ssaVersion = ssaVersion;
	}
	public void setInstructionId(int instructionId) {
		this.instructionId = instructionId;
	}
	public void setIdentifierName(String identifierName) {
		this.identifierName = identifierName;
	}
	public String getIdentifierName() {
		return identifierName;
	}
	public Type getType() {
		return type;
	}
	public void setType(Type type) {
		this.type = type;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public int getAddress() {
		return address;
	}
	public void setAddress(int address) {
		this.address = address;
	}
	public int getRegisterNumber() {
		return registerNumber;
	}
	public void setRegisterNumber(int registerNumber) {
		this.registerNumber = registerNumber;
	}
	public Token getRelationOperator() {
		return relationOperator;
	}
	public void setRelationOperator(Token relationOperator) {
		this.relationOperator = relationOperator;
	}
	public int getFixupLocation() {
		return fixupLocation;
	}
	public void setFixupLocation(int fixupLocation) {
		this.fixupLocation = fixupLocation;
	}
	
	public int getFunctionBasicBlock() {
		return functionBasicBlock;
	}
	public void setFunctionBasicBlock(int functionBasicBlock) {
		this.functionBasicBlock = functionBasicBlock;
	}
	public int getParameterCount() {
		return parameterCount;
	}
	public void setParameterCount(int parameterCount) {
		this.parameterCount = parameterCount;
	}
	public int getSsaVersion() {
		return ssaVersion;
	}
	@Override
	public String toString() {
		switch (this.type) {
			case VARIABLE:
				return identifierName + "_" + ssaVersion;
			case CONDITION:
				return relationOperator+"";
			case CONSTANT:
				return "#"+value+"";
			case INSTRUCTION:
				return "(" + instructionId + ")";
			case REGISTER:
				return "R_" + registerNumber +"";
			case BRANCHINSTR:
				return "[" + targetBasicBlock + "]";
			case ARRAYVAR:
				return identifierName;
			case BASEADDRESS:
				return identifierName+"_baseaddress";
			case FRAMEPOINTER:
				return "FP";
			case FUNCTION:
				return "["+functionBasicBlock+"]";
			case PARAMETER_COUNT:
				return "p"+parameterCount;
			default:
				return super.toString();
		}
	}	
}
