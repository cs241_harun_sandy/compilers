package edu.uci.compilers.dataTypes;

import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class InterferenceGraph {
	private Set<Instruction> instructions;
	private Set<Instruction> phiInstructions;
//	private Set<Edge> edges;
	private Map<Result, Set<Result>> adjacencyList;
	
	
	public Set<Instruction> getPhiInstructions() {
		return phiInstructions;
	}
	public void setPhiInstructions(Set<Instruction> phiInstructions) {
		this.phiInstructions = phiInstructions;
	}
	public Set<Instruction> getInstructions() {
		return instructions;
	}
	public void setInstructions(Set<Instruction> instructions) {
		this.instructions = instructions;
	}
//	public Set<Edge> getEdges() {
//		return edges;
//	}
//	public void setEdges(Set<Edge> edges) {
//		this.edges = edges;
//	}
	public void setAdjacencyList(Map<Result, Set<Result>> adjacencyList) {
		this.adjacencyList = adjacencyList;
	}
	public Map<Result, Set<Result>> getAdjacencyList() {
		return adjacencyList;
	}
	
	
	public void removeNode(Result node) {
		//First Remove the node from its neighbors' adjacency list.
		Set<Result> neighborList = adjacencyList.get(node);
		for(Result neighbor : neighborList){
			adjacencyList.get(neighbor).remove(node);
		}
		//Then remove the node
		adjacencyList.remove(node);
		
	}
	public boolean isEmpty() {
		return adjacencyList.isEmpty();
	}
	public Set<Result> getNeighbors(Result node) {
		return adjacencyList.get(node);
	}
	public void addNode(Result node, Set<Result> neighbors) {
		//First add the node to the neighbors' adjacency list
		for(Result neighbor : neighbors){
			if(adjacencyList.get(neighbor) == null){//because before we add edges the adjacency list will become empty
				adjacencyList.put(neighbor, new HashSet<Result>());
			}
			adjacencyList.get(neighbor).add(node);
		}
		adjacencyList.put(node, neighbors);
	}

	public void printAdjList(){
		System.out.println("The adjacency list is:");
		for(Entry<Result, Set<Result>> keyVal: adjacencyList.entrySet()){
			Result key = keyVal.getKey();
			Set<Result> val = keyVal.getValue();
			System.out.println(key + ": " + val);
		}
	}
	public void printPhiInstructions(){
		System.out.println("The Phi Instructions are:");
		for(Instruction phiInstruction: phiInstructions){
			System.out.println(phiInstruction);
		}
	}
}
