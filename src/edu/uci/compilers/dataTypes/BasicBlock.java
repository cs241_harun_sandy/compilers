package edu.uci.compilers.dataTypes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class BasicBlock {
	public enum BasicBlockType{
		NORMAL("NORMAL"),
		IF("IF"),
		ELSE("ELSE"),
		WHILE("WHILE"),
		RETURN("RETURN"),
		IF_JOIN("IF_JOIN"),
		IF_ELSE_JOIN("IF_ELSE_JOIN"),
		WHILEBODY("WHILE_BODY");
		private String name;
		private BasicBlockType(String name){
			this.name = name;
		}
		public String getName(){
			return this.name;
		}
	}
	private BasicBlockType type;
	private List<Instruction> instructions;
	private List<BasicBlock> children;
	private List<BasicBlock> parents;
	private BasicBlock functionBlock;//applicable only to function
	private static int numBasicBlocks;
	private int basicBlockId;
	private Map<String, Integer> ssaMap;
	private List<Function> functionsCalled;
	private GlobalVariableManager gvm;
	private BasicBlock bottomUpMergerBasicBlock;//to accommadate LiveRangeAnalysis of If-Else, Lca is tough as CFG is not a DAG
	
	public BasicBlock getBottomUpMergerBasicBlock() {
		return bottomUpMergerBasicBlock;
	}
	public void setBottomUpMergerBasicBlock(BasicBlock bottomUpMergerBasicBlock) {
		this.bottomUpMergerBasicBlock = bottomUpMergerBasicBlock;
	}
	private Set<Result> liveSet;
	
	public Set<Result> getLiveSet() {
		return liveSet;
	}
	
	public void setLiveSet(Set<Result> liveSet) {
		this.liveSet = liveSet;
	}
	
	public void setFunctionsCalled(List<Function> functionsCalled) {
		this.functionsCalled = functionsCalled;
	}
	public List<Function> getFunctionsCalled() {
		return functionsCalled;
	}
	public void addFunctionsCalled(Function function){
		this.functionsCalled.add(function);
	}
	public void setSsaMap(Map<String, Integer> variableVersions) {
		this.ssaMap = variableVersions;
	}
	public Map<String, Integer> getSsaMap() {
		return ssaMap;
	}
	public void setType(BasicBlockType type) {
		this.type = type;
	}
	public BasicBlockType getType() {
		return type;
	}
	public int getBasicBlockId() {
		return basicBlockId;
	}
	public void addBasicBlockChildren(BasicBlock basicBlock){
		this.children.add(basicBlock);
		Utils.deepCopySsa(this, basicBlock);
	}
//	public void addBasicBlockParent(BasicBlock basicBlock){//no longer used!
//		parents.add(basicBlock);		
//	}
	
	public BasicBlock(){
		children=new ArrayList<BasicBlock>();
		parents=new ArrayList<BasicBlock>();
		instructions=new ArrayList<Instruction>();
		ssaMap = new HashMap<String, Integer>();
		this.basicBlockId=numBasicBlocks;
		this.functionsCalled = new ArrayList<Function>();
		this.liveSet = new HashSet<Result>();
		gvm = GlobalVariableManager.getInstance();
		this.bottomUpMergerBasicBlock = null;//will be updated during parsing
		numBasicBlocks++;
	}
	public BasicBlock(BasicBlockType basicBlockType){
		this();
		this.type = basicBlockType;		
	}
	public void addInstruction(Instruction instruction){
		this.instructions.add(instruction);
	}
	
	public List<Instruction> getInstructions() {
		return instructions;
	}

	public void setInstructions(List<Instruction> instructions) {
		this.instructions = instructions;
	}

	public List<BasicBlock> getChildren() {
		return children;
	}

	public void setChildren(List<BasicBlock> children) {
		this.children = children;
	}

	public List<BasicBlock> getParents() {
		return parents;
	}

	public void setParents(List<BasicBlock> parents) {
		this.parents = parents;
	}
	
	@Override
	public String toString() {
		return "Id: " + basicBlockId + instructions.toString();
	}
	
	public void addToSsaMap(String identifier){
		ssaMap.put(identifier, 0);
		gvm.addVariable(identifier);
	}
	public void updateSsaMapOfVariableWithGlobalVariableManager(Result lhs) {
		String identifier = lhs.getIdentifierName();
		if(ssaMap.get(identifier)==null){
			System.err.println("Variable not found in local SSA map");
		}
		GlobalVariableManager gvm = GlobalVariableManager.getInstance();
		ssaMap.put(identifier, gvm.getLatestSSAVersion(identifier));
	}
	public Integer getSsaMapOfVaribale(String identifier) {
		return ssaMap.get(identifier);
	}

	public void updateLocalSsaMap(String identifier, Integer ssaVersion) {
		ssaMap.put(identifier, ssaVersion);
	}
	
}
