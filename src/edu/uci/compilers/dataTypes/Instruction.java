package edu.uci.compilers.dataTypes;

import edu.uci.compilers.dataTypes.Result.Type;

public class Instruction {
	private int id;
	private Operation operation;
	private Result operand1;
	private Result operand2;
	private Result operand3; //only for PHI instrs
	
	private Result arrayVar;//for load store instructions
	private int resultRegisterNumber;// -1 until a register is allocated
	
	private static int numInstructions;
	
	public Result getArrayVar() {
		return arrayVar;
	}
	public void setArrayVar(Result arrayVar) {
		this.arrayVar = arrayVar;
	}
	public static int getNumInstructions(){
		return numInstructions;
	}
	
	public Instruction(){
		this.setId(numInstructions);
		numInstructions ++;
		operation = null;
		operand1 = null;
		operand2 = null;
		operand3 = null;
		arrayVar = null;
		resultRegisterNumber = -1;
	}
	
	@Override
	public boolean equals(Object obj){
		Instruction instr = (Instruction) obj;
		boolean eq = (this.operation == instr.getOperation());
		boolean op1eq = false, op2eq = false, op3eq = false, arrVarEq = false;
		
		if(this.operand1 == null && instr.getOperand1() == null) op1eq = true;
		else if(this.operand1 != null && instr.getOperand1() == null) op1eq = false;
		else if(this.operand1 == null && instr.getOperand1() != null) op1eq = false;
		else if(this.operand1.equals(instr.getOperand1())) op1eq = true;
		else op1eq = false;
		
		if(this.operand2 == null && instr.getOperand2() == null) op2eq = true;
		else if(this.operand2 != null && instr.getOperand2() == null) op2eq = false;
		else if(this.operand2 == null && instr.getOperand2() != null) op2eq = false;
		else if(this.operand2.equals(instr.getOperand2())) op2eq = true;
		else op2eq = false;

		if(this.operand3 == null && instr.getOperand3() == null) op3eq = true;
		else if(this.operand3 != null && instr.getOperand3() == null) op3eq = false;
		else if(this.operand3 == null && instr.getOperand3() != null) op3eq = false;
		else if(this.operand3.equals(instr.getOperand3())) op3eq = true;
		else op3eq = false;
		
		if(this.arrayVar == null && instr.getArrayVar() == null)  arrVarEq = true;
		else if(this.arrayVar == null && instr.getArrayVar() != null) arrVarEq = false;
		else if(this.arrayVar != null && instr.getArrayVar() == null) arrVarEq = false;
		else if(this.arrayVar.equals(instr.getArrayVar())) arrVarEq = true;
		else arrVarEq = false;
		
		return eq && op1eq && op2eq && op3eq && arrVarEq;
	}
	
	@Override
	public int hashCode(){
		return 13 * this.id;
	}
	
	public Result getOperand3() {
		return operand3;
	}
	public void setOperand3(Result operand3) {
		this.operand3 = operand3;
	}
	public Operation getOperation() {
		return operation;
	}
	public void setOperation(Operation operation) {
		this.operation = operation;
	}
	public Result getOperand1() {
		return operand1;
	}
	public void setOperand1(Result operand1) {
		this.operand1 = operand1;
	}
	public Result getOperand2() {
		return operand2;
	}
	public void setOperand2(Result operand2) {
		this.operand2 = operand2;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getId() {
		return id;
	}
	public void setRegNo(int regNo) {
		this.resultRegisterNumber = regNo;
	}
	public int getRegNo() {
		return resultRegisterNumber;
	}
	@Override
	public String toString() {
		String regOrId;
		if(resultRegisterNumber != -1){
			regOrId = "R_" + resultRegisterNumber;
		} else{
			regOrId = "" + id;
		}
		if(operation==null) 
			return "";
		if(operation == Operation.READ){
			return "(" + regOrId + "): " + "read";
		}
		if(operation == Operation.PROPAGATE){
			return "(" + regOrId + "): " + "prop " + operand1;
		}
		if(operation == Operation.WRITE){
			return "(" + regOrId + "): " + "write " + operand1;
		}
		if(operation == Operation.WRITENL){
			return "(" + regOrId + "): " + "writeNL";
		}
		if(operation == Operation.END){
			return "(" + regOrId + "): " + Operation.END.getName();
		}
		if(operation==Operation.PHI){
			return "(" + regOrId + "): " + operation.getName() + " " + operand1 + " " + operand2 + " " + operand3;
		}
		if(operation == Operation.RET || operation == Operation.BRA || 
				operation == Operation.PARAM){
			return "(" + regOrId + "): " + operation.getName() + " " + operand1;
		}
		if(operation == Operation.LOAD){
			return "(" + regOrId + "): " + operation.getName() + " " + operand1 + 
					" {" + arrayVar.getIdentifierName() + "}";
		}
		if(operation == Operation.STORE){
			return "(" + regOrId + "): " + operation.getName() + " " + operand1 + " " + operand2 +
					" {" + arrayVar.getIdentifierName() + "}";
		}
		if(operation == Operation.DUMMY_KILL){
			return "(" + regOrId + "): " + operation.getName() + " " + arrayVar;
		}
		return "(" + regOrId + "): " + operation.getName() + " " + operand1 + " " + operand2;
	}
	
//	debug purposes
//	public static void main(String[] args){
//		Instruction in1 = new Instruction();
//		in1.setOperation(Operation.READ);
//		Result r1 = new Result();
//		r1.setIdentifierName("hello");
//		r1.setType(Type.VARIABLE);
//		in1.setOperand1(r1);
//		Instruction in2 = new Instruction();
//		in2.setOperation(Operation.READ);
//		
//		Result r2 = new Result();
//		r2.setIdentifierName("hello");
//		r2.setType(Type.CONSTANT);
//		in2.setOperand1(r2);
//		System.out.println(in1.equals(in2));
//	}
}
