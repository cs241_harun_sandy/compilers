package edu.uci.compilers.dataTypes;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class Utils {
	public static void deepCopySsa(BasicBlock parentBasicBlock, BasicBlock childBasicBlock){
		//copies ssa from parentBasicBlock to childBasicBlock
		Map<String, Integer> copyMap = deepCopySsaVersionMap(parentBasicBlock.getSsaMap());
		childBasicBlock.setSsaMap(copyMap);
	}
	public static Map<String, Integer> deepCopySsaVersionMap(Map<String, Integer> variablesVsSSAVersion) {
		Map<String, Integer> copyMap = new HashMap<String, Integer>();
		for(Entry<String, Integer> entry : variablesVsSSAVersion.entrySet()){
			copyMap.put(entry.getKey(), entry.getValue());
		}
		return copyMap;
	}

}
