package edu.uci.compilers.dataTypes;

public class ErrorMessage {
	public static final String MAIN_NOT_FOUND="Main not found";
	public static final String PERIOD_NOT_FOUND="Period not found";
	public static final String OPENBRACKET_NOT_FOUND="Open bracket not found";
	public static final String CLOSEBRACKET_NOT_FOUND="Close bracket not found";
	public static final String COMMA_NOT_FOUND="Comma not found";
	public static final String OPENPAREN_NOT_FOUND="Open parenthesis not found";
	public static final String CLOSEPAREN_NOT_FOUND="Close parenthesis not found";
	public static final String BEGIN_NOT_FOUND="Begin:{ not found";
	public static final String END_NOT_FOUND="End:} not found";
	public static final String EXPRESSION_EXPECTED="Expecting an expression here";
	public static final String IDENTIFIER_EXPECTED="Expecting an identifier here";
	public static final String BECOMES_EXPECTED="Expecting a becomes: <- token here";
	public static final String IF_EXPECTED="If expected";
	public static final String THEN_EXPECTED="Then expected";
	public static final String FI_EXPECTED="FI expected";
	public static final String OD_EXPECTED="OD expected";
	public static final String DO_EXPECTED="Do expected";
	public static final String SEMI_EXPECTED="Expecting a semi-colon";
	public static final String STATEMENT_EXPECTED="Expecting a statement";
	public static final String RELATIONAL_OPERATOR_NOT_FOUND="Relational operator not found";
	public static final String FACTOR_EXPECTED="Factor expected";
	public static final String TERM_EXPECTED="Term expected";
	public static final String NUMBER_EXPECTED="Expecting a number";
	public static final String FUNCTION_OR_PROCEDURE_EXPECTED="Expecting a function or procedure keyword";
	public static final String INDEXOUTOFBOUNDS="Array Index not within limits";
	public static final String ARRAYEXPECTED="Expecting an array dimension";
	public static final String ARRAYDIMENSIONMISMATCH="Array dimensions mismatch";
	public static final String VARIABLE_NOT_DEFINED="Variable not defined";
	public static final String NOT_A_SPECIAL_FUNCTION="Not a special function";
	public static final String PARAMETER_EXPECTED="Expecting a parameter here";
	public static final String PARAMETER_NOT_EXPECTED="Parameter not expected here";
	public static final String LVALUE_REQUIRED="lvalue required";
	public static final String OP_CODE_NOT_WITHIN_THE_RANGE = "OpCode is not within the range";
	public static final String REGISTER_NOT_WITHIN_RANGE = "Register Number not within the range";
	public static final String LITERAL_NOT_WITHIN_RANGE = "The literal is not within the range";
	public static final String C_ABSOLUTE_NOT_WITHIN_THE_RANGE = "c absolute is not within the range";
	public static final String WRONG_FIXUP_LOCATION = "The fix up location should be in the range of program code list";
	
	
}
