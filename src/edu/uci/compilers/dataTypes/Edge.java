package edu.uci.compilers.dataTypes;


public class Edge {
	private Result vertex1;
	private Result vertex2;
	public Result getVertex1() {
		return vertex1;
	}
	public void setVertex1(Result vertex1) {
		this.vertex1 = vertex1;
	}
	public Result getVertex2() {
		return vertex2;
	}
	public void setVertex2(Result vertex2) {
		this.vertex2 = vertex2;
	}
	
	
}