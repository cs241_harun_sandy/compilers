package edu.uci.compilers.dataTypes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.sun.swing.internal.plaf.basic.resources.basic;
import com.sun.xml.internal.ws.developer.MemberSubmissionAddressing.Validation;

import edu.uci.compilers.cfg.ControlFlowGraph;
import edu.uci.compilers.cfg.IntermediateCodeGenerator;
import edu.uci.compilers.dataTypes.BasicBlock.BasicBlockType;
import edu.uci.compilers.dataTypes.Result.Type;

public class PhiFunctionManager {
	private IntermediateCodeGenerator icg;
	private ControlFlowGraph cfg;
	private GlobalVariableManager gvm;
	
	public void setCfg(ControlFlowGraph cfg) {
		this.cfg = cfg;
	}
	public PhiFunctionManager(IntermediateCodeGenerator icg){
		this.icg = icg;
		gvm = GlobalVariableManager.getInstance();
	}
	public PhiFunctionManager(){
		gvm = GlobalVariableManager.getInstance();
	}
	public void removePhiFunctions(){
		BasicBlock startBlock = cfg.getStartBlock();
		Set<BasicBlock> visited = new HashSet<BasicBlock>();
		removePhiFunctionsUtil(startBlock, visited);
		for(Entry<String, Function> keyVal: gvm.getFunctions().entrySet()){
			removePhiFunctionsUtil(keyVal.getValue().getStartBlock(), visited);
		}
	}
	
	private void removePhiFunctionsUtil(BasicBlock basicBlock, Set<BasicBlock> visited) {//normal dfs
		if(visited.contains(basicBlock)) return;
		visited.add(basicBlock);
		List<BasicBlock> parents = basicBlock.getParents();
		List<Instruction> phiInstructions = getPhiInstructions(basicBlock);
		List<Instruction> leftMoveInstructions = generateLeftMoveInstructions(phiInstructions);
		List<Instruction> rightMoveInstructions = generateRightMoveInstructions(phiInstructions);
		if(phiInstructions.size() > 0 && basicBlock.getType() == BasicBlockType.IF_JOIN){
			BasicBlock ifPathBlock = getIfPathBlock(parents);
			BasicBlock headerPathBlock = getElsePathBlock(parents);
			addMoveForPhiRemoval(ifPathBlock, leftMoveInstructions);
			addMoveForPhiRemoval(headerPathBlock, rightMoveInstructions);
		} 
		else if (phiInstructions.size() > 0 && basicBlock.getType() == BasicBlockType.IF_ELSE_JOIN){
			BasicBlock ifPathBlock = getIfPathBlock(parents);
			BasicBlock elsePathBlock = getElsePathBlock(parents);
			addMoveForPhiRemoval(ifPathBlock, leftMoveInstructions);
			addMoveForPhiRemoval(elsePathBlock, rightMoveInstructions);
		}
		else if(phiInstructions.size() > 0 && basicBlock.getType() == BasicBlockType.WHILE){
			BasicBlock bodyBlock = getWhileBodyPathBlock(parents);
			BasicBlock headerBlock = getHeaderPathBlock(parents);
			addMoveForPhiRemoval(headerBlock, leftMoveInstructions);
			addMoveForPhiRemoval(bodyBlock, rightMoveInstructions);
		}
		removePhiInstructionsFromBasicBlock(basicBlock);
		List<BasicBlock> children = basicBlock.getChildren();
		for(BasicBlock child: children){
			removePhiFunctionsUtil(child, visited);
		}
	}	
	private void removePhiInstructionsFromBasicBlock(BasicBlock basicBlock) {
		Iterator<Instruction> iter = basicBlock.getInstructions().iterator();
		while(iter.hasNext()){
			Instruction instr = iter.next();
			if(instr.getOperation() == Operation.PHI){
				iter.remove();
			}
		}
	}
	private void addMoveForPhiRemoval(BasicBlock basicBlock, List<Instruction> moveInstructions) {
		List<Instruction> allInstructions = basicBlock.getInstructions();
		if(allInstructions.size() > 0){// first check if final instruction is a branch. if yes add before that. if not add at the end
			Instruction instr = allInstructions.get(allInstructions.size() - 1);
			boolean saveInstr = false;
			if(isBranchInstr(instr)){
				saveInstr = true;
				allInstructions.remove(allInstructions.size() - 1);
			}
			allInstructions.addAll(moveInstructions);
			if(saveInstr)
				allInstructions.add(instr);
		} else{
			allInstructions.addAll(moveInstructions);
		}
		basicBlock.setInstructions(allInstructions);
	}
	private boolean isBranchInstr(Instruction instr) {
		Operation op = instr.getOperation();
		if(op == Operation.BRA || op == Operation.BNE || op == Operation.BEQ || op == Operation.BLE ||
				op == Operation.BLT || op == Operation.BGE || op == Operation.BGT){
			return true;
		}
		return false;
	}
	private List<Instruction> generateRightMoveInstructions(List<Instruction> phiInstructions) {
		List<Instruction> rightMoveInstructions = new ArrayList<Instruction>();
		for(Instruction phiInstr: phiInstructions){//now all are registers
			Result phiResult = phiInstr.getOperand1();
			Result phiRightResult = phiInstr.getOperand3();
			if(phiRightResult.getType() == Type.REGISTER){
				if(phiRightResult.getRegisterNumber() != phiResult.getRegisterNumber()){//only then you move
					Instruction rightMove = new Instruction();
					rightMove.setOperation(Operation.MOVE);
					rightMove.setOperand1(phiRightResult);
					rightMove.setOperand2(phiResult);
					rightMoveInstructions.add(rightMove);
				}
			}
		}
		return rightMoveInstructions;
	}
	private List<Instruction> generateLeftMoveInstructions(List<Instruction> phiInstructions) {
		List<Instruction> leftMoveInstructions = new ArrayList<Instruction>();
		for(Instruction phiInstr: phiInstructions){//now all are registers
			Result phiResult = phiInstr.getOperand1();
			Result phiLeftResult = phiInstr.getOperand2();
			if(phiLeftResult.getType() == Type.REGISTER){
				if(phiLeftResult.getRegisterNumber() != phiResult.getRegisterNumber()){//only then you move
					Instruction leftMove = new Instruction();
					leftMove.setOperation(Operation.MOVE);
					leftMove.setOperand1(phiLeftResult);
					leftMove.setOperand2(phiResult);
					leftMoveInstructions.add(leftMove);
				}
			}
		}
		return leftMoveInstructions;
	}
	private List<Instruction> getPhiInstructions(BasicBlock basicBlock) {
		List<Instruction> phiInstructions = new ArrayList<Instruction>();
		List<Instruction> allInstructions = basicBlock.getInstructions();
		for(Instruction instr: allInstructions){
			if(instr.getOperation() == Operation.PHI){
				phiInstructions.add(instr);
			}
		}
		return phiInstructions;
	}
	private BasicBlock getWhileBodyPathBlock(List<BasicBlock> parents) {
		BasicBlock whileBodyPathBlock = null;
		for(BasicBlock parent: parents){ // if the last instr is uncond branch, then its the while body path
			List<Instruction> instructions = parent.getInstructions();
			if(instructions.size() == 0) continue;
			Instruction instr = instructions.get(instructions.size()-1);
			Operation op = instr.getOperation();
			if(op == Operation.BRA){
				whileBodyPathBlock = parent;
				break;
			}
		}
		return whileBodyPathBlock;
	}

	private BasicBlock getHeaderPathBlock(List<BasicBlock> parents) {// assumption - all basic blocks have at most 2 parents
		BasicBlock headerPathBlock = null;
		BasicBlock whileBodyPathBlock = getWhileBodyPathBlock(parents);
		for(BasicBlock parent: parents){
			if(parent != whileBodyPathBlock){
				headerPathBlock = parent;
				break;
			}
		}
		return headerPathBlock;
	}

	private BasicBlock getElsePathBlock(List<BasicBlock> parents) {
		BasicBlock elsePathBlock = null;
		BasicBlock ifPathBlock = getIfPathBlock(parents);
		for(BasicBlock parent: parents){
			if(parent != ifPathBlock){
				elsePathBlock = parent;
				break;
			}
		}
		return elsePathBlock;
	}

	private BasicBlock getIfPathBlock(List<BasicBlock> parents) {
		BasicBlock ifPathBlock = null;
		for(BasicBlock parent: parents){
			List<Instruction> instructions = parent.getInstructions();
			if(instructions.size() == 0){
				ifPathBlock = parent;
				break;
			} else {
				Instruction instr = instructions.get(instructions.size()-1);//if final instruction is an uncond branch(BRA) then this is an if
				Operation op = instr.getOperation();
				if(op == Operation.BRA){
					ifPathBlock = parent;
					break;
				}
			}
		}
		return ifPathBlock;
	}

	public void createPhiFunctionsAndLocalSsaMap(BasicBlock mergeBlock, BasicBlock leftBlock,
			BasicBlock rightBlock){//creates and inserts phi function and also generates the merged ssa map
		Map<String, Integer> leftSsaMap = leftBlock.getSsaMap();
		Map<String, Integer> rightSsaMap = rightBlock.getSsaMap();
		Map<String, Integer> mergedSsaMap = new HashMap<String, Integer>();
		//insert phi functions and variables that are present only in leftSsaMap. like leftOuterJoin
		for(Entry<String, Integer> keyVal: leftSsaMap.entrySet()){
			String leftIdentifier = keyVal.getKey();
			Integer leftSsaVersion = keyVal.getValue();
			boolean isPhid = false;
			for(Entry<String, Integer> keyVal2: rightSsaMap.entrySet()){
				String rightIdentifier = keyVal2.getKey();
				Integer rightSsaVersion = keyVal2.getValue(); 
				if(leftIdentifier.equals(rightIdentifier) && !leftSsaVersion.equals(rightSsaVersion)){//if they were redefined only then we need Phi function
					
					Result leftRes = new Result();
					leftRes.setType(Type.VARIABLE);
					leftRes.setIdentifierName(leftIdentifier);
					leftRes.setSsaVersion(leftSsaVersion);
					
					Result rightRes = new Result();
					rightRes.setType(Type.VARIABLE);
					rightRes.setIdentifierName(rightIdentifier);
					rightRes.setSsaVersion(rightSsaVersion);
					
					Result newSsaVersion = icg.insertPhiFunctionIntoBlock(mergeBlock, leftRes, rightRes);
					isPhid = true;
					
					mergedSsaMap.put(leftIdentifier, newSsaVersion.getSsaVersion());
					break;
				}
			}
			if(!isPhid){
				mergedSsaMap.put(leftIdentifier, leftSsaVersion);
			}
		}
		//now insert variables only present in the rightSsaMap
		for(Entry<String, Integer> keyVal: rightSsaMap.entrySet()){
			String rightIdentifier = keyVal.getKey();
			Integer rightSsaVersion = keyVal.getValue();
			
			boolean isPhid = false;
			for(Entry<String, Integer> keyVal2: leftSsaMap.entrySet()){
				String leftIdentifier = keyVal2.getKey();
				Integer leftSsaVersion = keyVal2.getValue();
				
				if(leftIdentifier.equals(rightIdentifier)){
					isPhid=true;
					break;
				}
			}
			if(!isPhid){//not yet in the mergedSsaMap
				mergedSsaMap.put(rightIdentifier, rightSsaVersion);
			}
		}
		
		mergeBlock.setSsaMap(mergedSsaMap);
	}

	public void insertPhiFunctionsForWhile(BasicBlock whileBlock, BasicBlock basicBlock,
			BasicBlock whileBodyBlock, BasicBlock whileBodyEndBlock) {
		Map<String, Integer> leftSsaMap = basicBlock.getSsaMap();
		Map<String, Integer> rightSsaMap = whileBodyEndBlock.getSsaMap();
		
		for(Entry<String, Integer> keyVal: leftSsaMap.entrySet()){
			String leftIdentifier = keyVal.getKey();
			Integer leftSsaVersion = keyVal.getValue();
			
			if(rightSsaMap.containsKey(leftIdentifier)){//need a phi
				Integer rightSsaVersion = rightSsaMap.get(leftIdentifier);

				if(leftSsaVersion.equals(rightSsaVersion)){//no need for phi
					continue;
				}
				Result leftResult = new Result();
				leftResult.setType(Type.VARIABLE);
				leftResult.setIdentifierName(leftIdentifier);
				leftResult.setSsaVersion(leftSsaVersion);
				
				Result rightResult = new Result();
				rightResult.setType(Type.VARIABLE);
				rightResult.setIdentifierName(leftIdentifier);//as both leftIdentifier and rightIdentifier names are same
				rightResult.setSsaVersion(rightSsaVersion);
				
				Result phiInstrResult = icg.insertPhiFunctionAtFront(whileBlock, leftResult, rightResult);
				icg.updatePhiFunctionResultsInLoopHeader(whileBlock, phiInstrResult, leftResult, rightResult);
				icg.updatePhiFunctionResultsInLoopBody(whileBodyBlock, phiInstrResult, leftResult, rightResult, whileBlock);
			}
			
		}
	}
	
//	public void makeLocalSsaMap(BasicBlock mergeBlock, BasicBlock leftBlock, BasicBlock rightBlock){
//		Map<String, Integer> leftSsaMap = leftBlock.getSsaMap();
//		Map<String, Integer> rightSsaMap = rightBlock.getSsaMap();
//		Map<String, Integer> mergedSsaMap = new HashMap<String, Integer>();
//		for(Entry<String, Integer> keyVal: leftSsaMap.entrySet()){
//			String leftIdentifier = keyVal.getKey();
//			Integer leftSsaVersion = keyVal.getValue();
//			for(Entry<String, Integer> keyVal2: rightSsaMap.entrySet()){
//				String rightIdentifier = keyVal2.getKey();
//				Integer rightSsaVersion = keyVal2.getValue();
//				if(leftIdentifier.equals(rightIdentifier)){
//					
//				} else {
//					
//				}
//			}
//		}
//	}
}
