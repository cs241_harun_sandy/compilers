package edu.uci.compilers.dataTypes;

public enum Token {
	ERROR(0), TIMES(1), DIV(2), PLUS(11), MINUS(12), EQL(20), NEQ(21), LSS(22),
	GEQ(23), LEQ(24), GTR(25), PERIOD(30), COMMA(31), OPENBRACKET(32), 
	CLOSEBRACKET(34), CLOSEPAREN(35), BECOMES(40), THEN(41), DO(42), OPENPAREN(50),
	NUMBER(60), IDENT(61), SEMI(70), END(80), OD(81), FI(82), ELSE(90), LET(100),
	CALL(101), IF(102), WHILE(103), RETURN(104), VAR(110), ARR(111), FUNC(112),
	PROC(113), BEGIN(150), MAIN(200), EOF(255),
	INIT(256);//INIT is not part of language but to suggest no token has yet taken its place
	
	private int id;
	private Token(int id){
		this.id=id;
	}
}
