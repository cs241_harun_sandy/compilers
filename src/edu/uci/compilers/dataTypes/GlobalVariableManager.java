package edu.uci.compilers.dataTypes;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GlobalVariableManager {
	private static final int DEFAULT_SSA_VERSION = 0;
	private Map<String, Integer> variablesVsSSAVersion;
	private Map<String, List<Integer>> arrayVariablesVsDimensions;// holds all array variables and their dimensions
	private static GlobalVariableManager globalVariableManager;
	private Map<String, Function> functions;
	
	public static GlobalVariableManager getInstance(){
		if(globalVariableManager == null){
			globalVariableManager = new GlobalVariableManager();
		}
		return globalVariableManager;
	}
	
	
	private GlobalVariableManager(){
		variablesVsSSAVersion=new HashMap<String, Integer>();
		arrayVariablesVsDimensions = new HashMap<String, List<Integer>>();
		functions = new HashMap<String, Function>();
	}
	
	public void addVariable(String identifier){
		if(variablesVsSSAVersion.get(identifier)!=null) return;
		variablesVsSSAVersion.put(identifier, DEFAULT_SSA_VERSION);
	}
	
	public void updateVariableSSAVersion(String identifier){
		Integer ssaVersion = variablesVsSSAVersion.get(identifier);
		if(ssaVersion !=null){
			variablesVsSSAVersion.put(identifier, ssaVersion + 1);
		} else {
			variablesVsSSAVersion.put(identifier, DEFAULT_SSA_VERSION);
		}
	}
	
	public Integer getLatestSSAVersion(String identifier){
		return variablesVsSSAVersion.get(identifier);
	}
	
	public Map<String, List<Integer>> getArrayVariables() {
		return arrayVariablesVsDimensions;
	}

	public void addArrayVariable(String identifier, List<Integer> dimensions) {
		arrayVariablesVsDimensions.put(identifier, dimensions);
	}

	public List<Integer> getArrayDimensions(String identifier) {
		return arrayVariablesVsDimensions.get(identifier);
	}
	public Map<String, Integer> getVariablesVsSSAVersion() {
		return variablesVsSSAVersion;
	}
	public Map<String, Function> getFunctions() {
		return functions;
	}
	
	public Function getFunction(String identifier){
		return functions.get(identifier);
	}
	
	public void addFunction(String identifier, Function function){
		functions.put(identifier, function);
	}
}
