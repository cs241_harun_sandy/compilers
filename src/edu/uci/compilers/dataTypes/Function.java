package edu.uci.compilers.dataTypes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Function {
	
	private static int numOfFunctions=0;
	private static final int DEFAULT_SSA_VERSION = 0;
	private String name;
	private Map<String, Integer> localVariablesVsSSAVersion;
	private Map<String, List<Integer>> localArrayVariablesVsDims;
	private List<String> parameters;
	private BasicBlock startBlock;
	private BasicBlock endBlock;
	private GlobalVariableManager gvm;
	private int functionId;
	
	public void setEndBlock(BasicBlock endBlock) {
		this.endBlock = endBlock;
	}
	public BasicBlock getEndBlock() {
		return endBlock;
	}
	public Function(String name){
		this.name = name;
		localVariablesVsSSAVersion = new HashMap<String, Integer>();
		parameters = new ArrayList<String>();
		startBlock = new BasicBlock();
		this.functionId = numOfFunctions;
		gvm = GlobalVariableManager.getInstance();
		numOfFunctions++;
	}

	public String getName() {
		return name;
	}
	public int getFunctionId() {
		return functionId;
	}
	public void setName(String name) {
		this.name = name;
	}

	public Map<String, Integer> getLocalVariablesVsSSAVersion() {
		return localVariablesVsSSAVersion;
	}

	public void addLocalVariable(String variable) {
		Integer ssaVersion = localVariablesVsSSAVersion.get(variable);
		if(ssaVersion == null){
			localVariablesVsSSAVersion.put(variable, DEFAULT_SSA_VERSION);
		}else{
			localVariablesVsSSAVersion.put(variable, ssaVersion + 1);
		}
		startBlock.addToSsaMap(variable);
	}
	
	public void updateSSA(String identifier, BasicBlock basicBlock){
		gvm.updateVariableSSAVersion(identifier);
		Integer ssaVersion = gvm.getLatestSSAVersion(identifier);
//		Integer ssaVersion = localVariablesVsSSAVersion.get(identifier);
		if(ssaVersion !=null){
			localVariablesVsSSAVersion.put(identifier, ssaVersion + 1);
		} else {
			localVariablesVsSSAVersion.put(identifier, DEFAULT_SSA_VERSION);
		}
		basicBlock.updateLocalSsaMap(identifier, ssaVersion);
	}
	
	public List<String> getParameters() {
		return parameters;
	}

	public void addParameter(String parameter) {
		this.parameters.add(parameter);
	}

	public BasicBlock getStartBlock() {
		return startBlock;
	}	
	
	public Map<String, List<Integer>> getLocalArrayVariablesVsDims() {
		return localArrayVariablesVsDims;
	}
	
	public void addLocalArrayVariable(String arrayVariable, List<Integer> dimensions) {
		if(localArrayVariablesVsDims==null) localArrayVariablesVsDims = new HashMap<String, List<Integer>>();
		localArrayVariablesVsDims.put(arrayVariable, dimensions);
	}

	public Integer getLatestSSA(String identifier, BasicBlock basicBlock) {
		return basicBlock.getSsaMapOfVaribale(identifier);
//		return localVariablesVsSSAVersion.get(identifier);
	}
}
