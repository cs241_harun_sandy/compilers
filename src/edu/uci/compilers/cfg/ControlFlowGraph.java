package edu.uci.compilers.cfg;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import edu.uci.compilers.dataTypes.BasicBlock;
import edu.uci.compilers.dataTypes.Function;
import edu.uci.compilers.dataTypes.GlobalVariableManager;


public class ControlFlowGraph {
	private BasicBlock startBlock;
	private BasicBlock endBlock;
	private GlobalVariableManager gvm;
	
	public void setEndBlock(BasicBlock endBlock) {
		this.endBlock = endBlock;
	}
	
	public BasicBlock getEndBlock() {
		return endBlock;
	}
	
	public ControlFlowGraph(){
		startBlock = new BasicBlock();
		gvm = GlobalVariableManager.getInstance();
	}

	public BasicBlock getStartBlock() {
		return startBlock;
	}

	public void setStartBlock(BasicBlock startBlock) {
		this.startBlock = startBlock;
	}
	public void print(BasicBlock basicBlock, Set<Integer> visited, Set<Integer> functionsVisited){
		if(basicBlock==null) return;
		if(visited.contains(basicBlock.getBasicBlockId())){
			return;
		} else {
			visited.add(new Integer(basicBlock.getBasicBlockId()));
		}
		System.out.println("Block_"+basicBlock.getBasicBlockId()+":\n"+basicBlock.getType()+"\n"+basicBlock.toString()+"\n");
		for(BasicBlock bbl: basicBlock.getChildren()){
			if(bbl!=null) print(bbl, visited, functionsVisited);
		}
		for(Function fn: basicBlock.getFunctionsCalled()){
			if(functionsVisited.contains(fn.getFunctionId())){
				continue;
			} else {
				BasicBlock startBlock = fn.getStartBlock();
				functionsVisited.add(fn.getFunctionId());
				print(startBlock, visited, functionsVisited);
			}
		}
	}
	public void printCFG(){
		Set<Integer> visited=new HashSet<Integer>();
		Set<Integer> functionsVisited = new HashSet<Integer>();
		print(getStartBlock(), visited, functionsVisited);
	}

	public void populateParents() {
		Set<BasicBlock> visitedBasicBlocks = new HashSet<BasicBlock>();
		dfsToPopulateParents(startBlock, visitedBasicBlocks);
		Map<String, Function> functionsMap = gvm.getFunctions();
		for(Entry<String, Function> keyVal: functionsMap.entrySet()){
			BasicBlock functionBlock = keyVal.getValue().getStartBlock();
			dfsToPopulateParents(functionBlock, visitedBasicBlocks);
		}
	}

	private void dfsToPopulateParents(BasicBlock basicBlock, Set<BasicBlock> visitedBasicBlocks) {
		if(visitedBasicBlocks.contains(basicBlock)) return;
		visitedBasicBlocks.add(basicBlock);
		for(BasicBlock child: basicBlock.getChildren()){
			List<BasicBlock> parents = child.getParents();
			if(parents == null) parents = new ArrayList<BasicBlock>();
			parents.add(basicBlock);
			child.setParents(parents);
		}
		for(BasicBlock child: basicBlock.getChildren()){
			dfsToPopulateParents(child, visitedBasicBlocks);
		}
	}
	
}
