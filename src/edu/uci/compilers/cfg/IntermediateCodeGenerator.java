package edu.uci.compilers.cfg;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import edu.uci.compilers.dataTypes.BasicBlock;
import edu.uci.compilers.dataTypes.Function;
import edu.uci.compilers.dataTypes.Instruction;
import edu.uci.compilers.dataTypes.Operation;
import edu.uci.compilers.dataTypes.Result;
import edu.uci.compilers.dataTypes.Result.Type;
import edu.uci.compilers.dataTypes.GlobalVariableManager;
import edu.uci.compilers.dataTypes.Token;
import static edu.uci.compilers.dataTypes.ErrorMessage.*;

public class IntermediateCodeGenerator {
	
	private HashMap<Token, Operation> tokenVsOperation;
	private GlobalVariableManager gvm;
	
	private HashMap<Token, Operation> negatedCompareOperation;
	private HashMap<Integer, Instruction> allInstructions;
	private static final int SIZEOFINT=4;
	private static final String FRAMEPOINTER = "fp";
	
	public IntermediateCodeGenerator(){
		tokenVsOperation = new HashMap<Token, Operation>();
		tokenVsOperation.put(Token.PLUS, Operation.ADD);
		tokenVsOperation.put(Token.MINUS, Operation.SUB);
		tokenVsOperation.put(Token.TIMES, Operation.MUL);
		tokenVsOperation.put(Token.DIV, Operation.DIV);
		
		gvm= GlobalVariableManager.getInstance();
		
		negatedCompareOperation=new HashMap<Token, Operation>();
		negatedCompareOperation.put(Token.EQL, Operation.BNE);
		negatedCompareOperation.put(Token.NEQ, Operation.BEQ);
		negatedCompareOperation.put(Token.LSS, Operation.BGE);
		negatedCompareOperation.put(Token.LEQ, Operation.BGT);
		negatedCompareOperation.put(Token.GTR, Operation.BLE);
		negatedCompareOperation.put(Token.GEQ, Operation.BLT);
		
		allInstructions = new HashMap<Integer, Instruction>();
	}
	
//	public int getLatestSSAVersion(String identifier){
//		return globalVariableManager.getLatestSSA(identifier);
//	}
//	public void addIdentifierToSSA(String identifier){
//		globalVariableManager.addVariable(identifier);
//	}
//	
//	public void addArrayVariable(String identifier, List<Integer> dimensions){
//		globalVariableManager.addArrayVariable(identifier, dimensions);
//	}
	
	public void generateICForAssignment(BasicBlock basicBlock, Result rhs, Result lhs, Function function, 
			Result lhsArrayRes, List<Instruction> storeKills){
		Instruction inst = new Instruction();
		if(lhs.getType()==Type.VARIABLE){
			inst.setOperation(Operation.MOVE);
			String lhsIdent = lhs.getIdentifierName();
			if(function == null){
				gvm.updateVariableSSAVersion(lhsIdent);
				lhs.setSsaVersion(gvm.getLatestSSAVersion(lhsIdent));
				basicBlock.updateSsaMapOfVariableWithGlobalVariableManager(lhs);
			}else{
				function.updateSSA(lhsIdent, basicBlock);
				lhs.setSsaVersion(gvm.getLatestSSAVersion(lhsIdent));
			}
			if(rhs.getType()==Type.VARIABLE){
				String rhsIdent = rhs.getIdentifierName();
				if(function==null){
					Integer ssaVer = basicBlock.getSsaMapOfVaribale(rhsIdent);
					if(ssaVer == null){
						ssaVer = gvm.getLatestSSAVersion(rhsIdent);
					}
					rhs.setSsaVersion(ssaVer);
				} else {
					Integer ssaVer = function.getLatestSSA(rhsIdent, basicBlock);
					if(ssaVer == null){
						ssaVer = gvm.getLatestSSAVersion(rhsIdent);
					}
					rhs.setSsaVersion(ssaVer);
				}
			}
		}else if(lhs.getType()==Type.INSTRUCTION){
			inst.setOperation(Operation.STORE);
			inst.setArrayVar(lhsArrayRes);
			if(storeKills != null)
				storeKills.add(inst);
		}
		inst.setOperand1(rhs);
		inst.setOperand2(lhs);
		addInstrToBasicBlock(basicBlock, inst);
	}
	
	public Result generateICForExpression(BasicBlock basicBlock, Result r1, Result r2, Token tokenOp){
		if(r1!=null && r2!=null){
			if(r1.getType()==Type.CONSTANT && r2.getType()==Type.CONSTANT){
				Result constant=new Result();
				constant.setType(Type.CONSTANT);
				int r1Val = r1.getValue();
				int r2Val = r2.getValue();
				if(tokenOp==Token.PLUS){
					constant.setValue(r1Val + r2Val);
				} else if(tokenOp==Token.MINUS){
					constant.setValue(r1Val - r2Val);
				} else if(tokenOp==Token.TIMES){
					constant.setValue(r1Val * r2Val);
				} else if(tokenOp==Token.DIV){
					constant.setValue(r1Val / r2Val);
				}
				return constant;
			} else {
				Instruction instruction = new Instruction();
				instruction.setOperand1(r1);
				instruction.setOperand2(r2);
				instruction.setOperation(tokenVsOperation.get(tokenOp));
//				basicBlock.addInstruction(instruction);
//				allInstructions.put(instruction.getId(), instruction);
				addInstrToBasicBlock(basicBlock, instruction);
				Result instrResult=new Result();
				instrResult.setType(Type.INSTRUCTION);
				instrResult.setInstructionId(instruction.getId());
				return instrResult;
			}
		} else if(r1!=null){
			return r1;
		} else {
			return r2;
		}
	}
	public void addInstrToBasicBlock(BasicBlock basicBlock, Instruction instruction){
		basicBlock.addInstruction(instruction);
		allInstructions.put(instruction.getId(), instruction);
	}
	
	public Result generateICforRelation(BasicBlock basicBlock, Result leftExp, Result condition,
			Result rightExp){
		
		Instruction compareInst = new Instruction();
		compareInst.setOperation(Operation.CMP);
		compareInst.setOperand1(leftExp);
		compareInst.setOperand2(rightExp);
		addInstrToBasicBlock(basicBlock, compareInst);
		
		Result compareResult = new Result();
		compareResult.setType(Type.INSTRUCTION);
		compareResult.setInstructionId(compareInst.getId());
		
		Result condFalseBranchResult = new Result();
		condFalseBranchResult.setType(Type.CONDITION);
		
		Instruction branchOnCondFalseInstr = new Instruction();
		branchOnCondFalseInstr.setOperation(negatedCompareOperation.get(condition.getRelationOperator()));
		branchOnCondFalseInstr.setOperand1(compareResult);
		branchOnCondFalseInstr.setOperand2(condFalseBranchResult);
		
		addInstrToBasicBlock(basicBlock, branchOnCondFalseInstr);
		
		Result fixUpInstrResult = new Result();
		fixUpInstrResult.setFixupLocation(branchOnCondFalseInstr.getId());
		
		return fixUpInstrResult;
	}
	
//	public void generateICForRelation(BasicBlock basicBlock, Result leftExp, 
//			Result condition, Result rightExp, BasicBlock fallThroughBlock, BasicBlock bodyBlock){
//		
//		Instruction compareInstr = new Instruction();
//		compareInstr.setOperation(Operation.CMP);
//		compareInstr.setOperand1(leftExp);
//		compareInstr.setOperand2(rightExp);
////		basicBlock.addInstruction(compareInstr);
////		allInstructions.put(compareInstr.getId(), compareInstr);
//		addInstrToBasicBlock(basicBlock, compareInstr);
//		
//		Result cmpResult=new Result();
//		cmpResult.setType(Type.INSTRUCTION);
//		cmpResult.setInstructionId(compareInstr.getId());
//
//		Result braNegationResult=new Result();
//		braNegationResult.setType(Type.BRANCHINSTR);
//		braNegationResult.setTargetBasicBlock(fallThroughBlock.getBasicBlockId());
//		
//		//if the IF condition fails take this branch
//		Instruction branchToJoinOrElseBlockInstr = new Instruction();
////		Operation op=negatedCompareOperation.get(tp);
//		branchToJoinOrElseBlockInstr.setOperation(negatedCompareOperation.get(condition.getRelationOperator()));
//		branchToJoinOrElseBlockInstr.setOperand1(cmpResult);
//		branchToJoinOrElseBlockInstr.setOperand2(braNegationResult);
////		basicBlock.addInstruction(branchToJoinOrElseBlockInstr);
////		allInstructions.put(branchToJoinOrElseBlockInstr.getId(), branchToJoinOrElseBlockInstr);
//		addInstrToBasicBlock(basicBlock, branchToJoinOrElseBlockInstr);
//		
//		Result braResult = new Result();
//		braResult.setType(Type.BRANCHINSTR);
//		braResult.setTargetBasicBlock(bodyBlock.getBasicBlockId());
//		
//		//if the condition is true then fall through. indicated with BRA instruction
//		Instruction fallThroughInstr = new Instruction();
//		fallThroughInstr.setOperation(Operation.BRA);
//		fallThroughInstr.setOperand1(braResult);
//		fallThroughInstr.setOperand2(null);
////		basicBlock.addInstruction(fallThroughInstr);
////		allInstructions.put(fallThroughInstr.getId(), fallThroughInstr);
//		addInstrToBasicBlock(basicBlock, fallThroughInstr);
//		
////		Instruction instruction2 = new Instruction();
////		instruction2.setOperation(negatedCompareOperation.get(condition.getType()));
////		instruction2.setOperand1(result);
////		instruction2.setOperand2(operand2);
//
//	}
//
	public void insertFallThroughBranch(BasicBlock basicBlock, BasicBlock targetBlock) {
		// insert "BRA [joinOrElseBlock's id]" into ifBlock
		Result result = new Result();
		result.setType(Type.BRANCHINSTR);
		result.setTargetBasicBlock(targetBlock.getBasicBlockId());
		
		Instruction fallThroughInstr = new Instruction();
		fallThroughInstr.setOperation(Operation.BRA);
		fallThroughInstr.setOperand1(result);
		fallThroughInstr.setOperand2(null);
//		basicBlock.addInstruction(fallThroughInstr);
//		allInstructions.put(fallThroughInstr.getId(), fallThroughInstr);
		addInstrToBasicBlock(basicBlock, fallThroughInstr);
	}

	public void insertEnd(BasicBlock basicBlock, ControlFlowGraph cfg) {
		Instruction endInst=new Instruction();
		endInst.setOperation(Operation.END);
		endInst.setOperand1(null);
		endInst.setOperand2(null);
		
		addInstrToBasicBlock(basicBlock, endInst);
		
		cfg.setEndBlock(basicBlock);
	}

	public Result generateICForArrayDimensions(BasicBlock basicBlock, List<Result> dimensionExps, List<Integer> dimensions) {
		Result addR=null;
		if(dimensionExps.size() != dimensions.size()){
			error(ARRAYDIMENSIONMISMATCH);
		}
		if(dimensionExps.size()==1){//1d array TODO - is this right?
			return dimensionExps.get(0);
		} else if(dimensionExps.size()>1){
			int mulDim=1;
			for(int dim: dimensions){
				mulDim*=dim;
			}
			int currDimension=0;
			
			Result r1 = dimensionExps.get(0);
			
			Result mul = new Result();
			mul.setType(Type.CONSTANT);
			mulDim/=dimensions.get(currDimension++);
			mul.setValue(mulDim);
			
			Instruction mulInstr = new Instruction();
			mulInstr.setOperation(Operation.MUL);
			mulInstr.setOperand1(mul);
			mulInstr.setOperand2(r1);
			
			addInstrToBasicBlock(basicBlock, mulInstr);
			
			Result r2 = dimensionExps.get(1);
			
			Result mul2 = new Result();
			mul2.setType(Type.CONSTANT);
			mulDim/=dimensions.get(currDimension++);
			mul2.setValue(mulDim);
			
			Instruction mulInstr2 = new Instruction();
			mulInstr2.setOperation(Operation.MUL);
			mulInstr2.setOperand1(mul2);
			mulInstr2.setOperand2(r2);
			
			addInstrToBasicBlock(basicBlock, mulInstr2);
			
			Result i1 = new Result();
			i1.setType(Type.INSTRUCTION);
			i1.setInstructionId(mulInstr.getId());
			
			Result i2 = new Result();
			i2.setType(Type.INSTRUCTION);
			i2.setInstructionId(mulInstr2.getId());
			
			Instruction addInstr = new Instruction();
			addInstr.setOperation(Operation.ADD);
			addInstr.setOperand1(i1);
			addInstr.setOperand2(i2);
			
			addInstrToBasicBlock(basicBlock, addInstr);
			
			Instruction prevAdd = addInstr;
			for(int dims=2; dims<dimensionExps.size(); dims++){
				Result firstR = dimensionExps.get(dims);
				
				Result secondR = new Result();
				secondR.setType(Type.CONSTANT);
				mulDim/=dimensions.get(currDimension++);
				secondR.setValue(mulDim);
				
				Instruction mulI = new Instruction();
				mulI.setOperation(Operation.MUL);
				mulI.setOperand1(firstR);
				mulI.setOperand2(secondR);
				
				addInstrToBasicBlock(basicBlock, mulI);
				
				Result mulR = new Result();
				mulR.setType(Type.INSTRUCTION);
				mulR.setInstructionId(mulI.getId());
				
				Result pAddR = new Result();
				pAddR.setType(Type.INSTRUCTION);
				pAddR.setInstructionId(prevAdd.getId());
				
				Instruction addI = new Instruction();
				addI.setOperation(Operation.ADD);
				addI.setOperand1(pAddR);
				addI.setOperand2(mulR);
				
				addInstrToBasicBlock(basicBlock, addI);
				
				prevAdd = addI;
			}
			addR = new Result();
			addR.setType(Type.INSTRUCTION);
			addR.setInstructionId(prevAdd.getId());
		} else {
			error(ARRAYEXPECTED);
		}
		return addR;
	}

	public Result generateICForArrayEnds(BasicBlock basicBlock, String identifier, Result currentDimExp, boolean fromAssignment) {
		
		Result sizeofInt = new Result();
		sizeofInt.setType(Type.CONSTANT);
		sizeofInt.setValue(SIZEOFINT);
				
		Instruction mulByIntSize = new Instruction();
		mulByIntSize.setOperation(Operation.MUL);
		mulByIntSize.setOperand1(currentDimExp);
		mulByIntSize.setOperand2(sizeofInt);
		
		addInstrToBasicBlock(basicBlock, mulByIntSize);
		
		Result mulByIntResult = new Result();
		mulByIntResult.setType(Type.INSTRUCTION);
		mulByIntResult.setInstructionId(mulByIntSize.getId());
		
		Result fp = new Result();
		fp.setType(Type.FRAMEPOINTER);
				
		Result arrayBaseAddr = new Result();
		arrayBaseAddr.setType(Type.BASEADDRESS);
		arrayBaseAddr.setIdentifierName(identifier);
		
		Instruction addFramePtr = new Instruction();
		addFramePtr.setOperation(Operation.ADD);
		addFramePtr.setOperand1(fp);
		addFramePtr.setOperand2(arrayBaseAddr);
		
		addInstrToBasicBlock(basicBlock, addFramePtr);
		
		Result arrayBaseResult = new Result();
		arrayBaseResult.setType(Type.INSTRUCTION);
		arrayBaseResult.setInstructionId(addFramePtr.getId());
		
		Instruction adda = new Instruction();
		adda.setOperation(Operation.ADDA);
		adda.setOperand1(mulByIntResult);
		adda.setOperand2(arrayBaseResult);
		
		addInstrToBasicBlock(basicBlock, adda);
		
		Result addaResult = new Result();
		addaResult.setType(Type.INSTRUCTION);
		addaResult.setInstructionId(adda.getId());
		
		if(!fromAssignment){
			Instruction loadAddress = new Instruction();
			loadAddress.setOperation(Operation.LOAD);
			loadAddress.setOperand1(addaResult);
			loadAddress.setOperand2(null);
			
			Result arrayVar = new Result();
			arrayVar.setType(Type.ARRAYVAR);
			arrayVar.setIdentifierName(identifier);
			
			loadAddress.setArrayVar(arrayVar);
			
			addInstrToBasicBlock(basicBlock, loadAddress);
			Result loadAddressResult = new Result();
			loadAddressResult.setType(Type.INSTRUCTION);
			loadAddressResult.setInstructionId(loadAddress.getId());
			return loadAddressResult;
		}
		return addaResult;
	}
	private void error(String mesg){
		System.out.println("Error while ICG: "+ mesg);
	}

	public List<Integer> getArrayDimensions(String identifier) {
		return gvm.getArrayDimensions(identifier);
	}

	public void generateICForReturnStatemnt(BasicBlock basicBlock, Result result) {
		Instruction returnInstruction = new Instruction();
		returnInstruction.setOperation(Operation.RET);
		returnInstruction.setOperand1(result);
		//2nd operand is null for RET instruction
		addInstrToBasicBlock(basicBlock, returnInstruction);
	}

	public void generateICForFunctionParam(BasicBlock basicBlock, Result paramResult) {//not used anymore
		Instruction paramInstruction = new Instruction();
		paramInstruction.setOperation(Operation.PARAM);
		paramInstruction.setOperand1(paramResult);
		//2nd operand is null for PARAM instruction
		addInstrToBasicBlock(basicBlock, paramInstruction);
		
	}

	public Result generateICForFunctionCall(BasicBlock basicBlock,
			Function function) {
		BasicBlock functionBlock = function.getStartBlock();
		
		Result operand1 = new Result();
		operand1.setType(Type.FUNCTION);
		operand1.setFunctionBasicBlock(functionBlock.getBasicBlockId());
		
		Result operand2 = new Result();
		operand2.setType(Type.PARAMETER_COUNT);
		operand2.setParameterCount(function.getParameters().size());

		Instruction callInstruction = new Instruction();
		callInstruction.setOperation(Operation.CALL);
		callInstruction.setOperand1(operand1);
		callInstruction.setOperand2(operand2);
		
		addInstrToBasicBlock(basicBlock, callInstruction);
		
		Result returnResult = new Result();
		returnResult.setType(Type.INSTRUCTION);
		returnResult.setInstructionId(callInstruction.getId());
		return returnResult;
		
		
	}

	public void fixUpLocation(Result fixup, BasicBlock basicBlock) {// fixes instruction pointed by fixup with the basicBlock's id
		int targetBasicBlock = basicBlock.getBasicBlockId();
		int instrId = fixup.getFixupLocation();
		
		Result fix = new Result();
		fix.setType(Type.BRANCHINSTR);
		fix.setTargetBasicBlock(targetBasicBlock);
		
		Instruction toFix = allInstructions.get(instrId);
		toFix.setOperand2(fix);
	}

	public Result insertPhiFunctionIntoBlock(BasicBlock mergeBlock, Result leftRes, Result rightRes) {

		gvm.updateVariableSSAVersion(leftRes.getIdentifierName());
		
		Result phiVariable = new Result();
		phiVariable.setType(Type.VARIABLE);
		phiVariable.setIdentifierName(leftRes.getIdentifierName());
		phiVariable.setSsaVersion(gvm.getLatestSSAVersion(leftRes.getIdentifierName()));
		
		Instruction phiInstr = new Instruction();
		phiInstr.setOperation(Operation.PHI);
		phiInstr.setOperand1(phiVariable);
		phiInstr.setOperand2(leftRes);
		phiInstr.setOperand3(rightRes);
		
		addInstrToBasicBlock(mergeBlock, phiInstr);
		
		return phiVariable;
	}

	public Result insertPhiFunctionAtFront(BasicBlock whileBlock, Result leftResult, Result rightResult) {
		
		gvm.updateVariableSSAVersion(leftResult.getIdentifierName());
		whileBlock.updateSsaMapOfVariableWithGlobalVariableManager(leftResult);
		
		Result phiVariable = new Result();
		phiVariable.setType(Type.VARIABLE);
		phiVariable.setIdentifierName(leftResult.getIdentifierName());
		phiVariable.setSsaVersion(gvm.getLatestSSAVersion(leftResult.getIdentifierName()));
		
		Instruction phiInstr = new Instruction();
		phiInstr.setOperation(Operation.PHI);
		phiInstr.setOperand1(phiVariable);
		phiInstr.setOperand2(leftResult);
		phiInstr.setOperand3(rightResult);
		
		addInstrAtFrontOfBasicBlock(whileBlock, phiInstr);
		
		return phiVariable;
	}

	private void addInstrAtFrontOfBasicBlock(BasicBlock basicBlock, Instruction phiInstr) {
		List<Instruction> instructions = basicBlock.getInstructions();
		instructions.add(0, phiInstr);
		basicBlock.setInstructions(instructions);
		allInstructions.put(phiInstr.getId(), phiInstr);
	}

	public void updatePhiFunctionResultsInLoopBody(BasicBlock whileBodyBlock, Result phiInstrResult,
			Result leftResult, Result rightResult, BasicBlock whileBlock){
		Set<BasicBlock> visitedBasicBlocks = new HashSet<BasicBlock>();
		visitedBasicBlocks.add(whileBlock);
		updatePhiFuncResultsInLoopBodyDFS(whileBodyBlock, phiInstrResult, leftResult, rightResult, visitedBasicBlocks);
	}
	
	public void updatePhiFuncResultsInLoopBodyDFS(BasicBlock whileBodyBlock, Result phiInstrResult,
			Result leftResult, Result rightResult,Set<BasicBlock> visitedBasicBlocks){
		//do a dfs based traversal instead of bfs
		
		if(visitedBasicBlocks.contains(whileBodyBlock)) return;
		visitedBasicBlocks.add(whileBodyBlock);
		
		String phiUpdatedIdentifier = phiInstrResult.getIdentifierName();
		Integer leftSsaVersion = leftResult.getSsaVersion();
		Integer rightSsaVersion = rightResult.getSsaVersion();
		Integer phiSsaVersion = phiInstrResult.getSsaVersion();

		List<Instruction> instructions = whileBodyBlock.getInstructions();
		//now update the instructions
		for(Instruction instr: instructions){
			Operation op = instr.getOperation();
			Result operand1 = instr.getOperand1();
			Result operand2 = instr.getOperand2();
			Result operand3 = instr.getOperand3();
			
			if(op == Operation.PHI) {
				if(operand1.getIdentifierName().equals(phiUpdatedIdentifier)){//new definition for the variable
					
					Result newPhiToUseFurther = operand1;
					
					String usedIdentifier2 = operand2.getIdentifierName();
					String usedIdentifier3 = operand3.getIdentifierName();
					
					Integer usedSsaVersion2 = operand2.getSsaVersion();
					Integer usedSsaVersion3 = operand3.getSsaVersion();
					
					if(usedIdentifier2.equals(phiUpdatedIdentifier)){
						if(usedSsaVersion2.equals(leftSsaVersion) || usedSsaVersion2.equals(rightSsaVersion)){
							operand2.setSsaVersion(phiSsaVersion);
						}
					}
					if(usedIdentifier3.equals(phiSsaVersion)){
						if(usedSsaVersion3.equals(leftSsaVersion) || usedSsaVersion3.equals(rightSsaVersion)){
							operand3.setSsaVersion(phiSsaVersion);
						}
					}//update the 
					phiInstrResult = newPhiToUseFurther;
					
					phiUpdatedIdentifier = phiInstrResult.getIdentifierName();
					phiSsaVersion = phiInstrResult.getSsaVersion();
					
					instr.setOperand2(operand2);
					instr.setOperand3(operand3);
				}
			} else {
				//first check if its a redefinition of phiInstrResult
				if(op == Operation.MOVE){
					//first check the used variable
					if(operand1.getIdentifierName().equals(phiUpdatedIdentifier)){
						if(operand1.getSsaVersion() == leftSsaVersion || operand1.getSsaVersion() == rightSsaVersion){//now propagate the phi
							operand1.setSsaVersion(phiSsaVersion);
						}
					}
					if(operand2.getIdentifierName().equals(phiUpdatedIdentifier)){//a redefinition. make sure you use this redifinition further
						phiInstrResult = operand2;
						
						phiUpdatedIdentifier = phiInstrResult.getIdentifierName();
						phiSsaVersion = phiInstrResult.getSsaVersion();
					}
					instr.setOperand1(operand1);//update the
				} else {
					if(operand1 != null && operand1.getType()==Type.VARIABLE){//check the use
						if(operand1.getIdentifierName().equals(phiUpdatedIdentifier)){//now use the latest definition of the variable
							operand1.setSsaVersion(phiSsaVersion);//irrespective of the version, we have to use phi updated version
						}
					}
					if(operand2 != null && operand2.getType()==Type.VARIABLE){
						if(operand2.getIdentifierName().equals(phiUpdatedIdentifier)){
							operand2.setSsaVersion(phiSsaVersion);//irrespective of version, we have to use phi updated version
						}
					}
					instr.setOperand1(operand1);
				}
			}
		}//all instructions in the basicblock are done, now recurse depth first order
		for(BasicBlock childBasicBlock: whileBodyBlock.getChildren()){
			updatePhiFuncResultsInLoopBodyDFS(childBasicBlock, phiInstrResult, leftResult, rightResult, visitedBasicBlocks);
		}
	}

	
//	public void updatePhiFunctionResultsInLoopBodyDeprecated(BasicBlock whileBodyBlock, Result phiInstrResult,
//			Result leftResult, Result rightResult, BasicBlock whileBlock) {
//		
//		String phiUpdatedIdentifier = phiInstrResult.getIdentifierName();
//		Integer leftSsaVersion = leftResult.getSsaVersion();
//		Integer rightSsaVersion = rightResult.getSsaVersion();
//		Integer phiSsaVersion = phiInstrResult.getSsaVersion();
//
//		Set<Integer> visited = new HashSet<Integer>();
//
//		Queue<BasicBlock> queue = new LinkedList<BasicBlock>();
//		queue.add(whileBodyBlock);
//		
//		while(!queue.isEmpty()){
//			BasicBlock currentBasicBlock = queue.poll();
//			if(currentBasicBlock.equals(whileBlock)){
//				break;
//			}
//			if(visited.contains(currentBasicBlock.getBasicBlockId())){
//				continue;
//			}
//			
//			visited.add(currentBasicBlock.getBasicBlockId());
//			
//			List<Instruction> instructions = currentBasicBlock.getInstructions();
//			List<Instruction> updatedInstructions = new ArrayList<Instruction>();
//			for(Instruction instr: instructions){
//				if(instr.getOperation()==Operation.PHI){
//					
//					Result newPhiToUseFurther = instr.getOperand1();
//					
//					Result operand2 = instr.getOperand2();
//					Result operand3 = instr.getOperand3();
//					
//					String usedIdentifier2 = operand2.getIdentifierName();
//					String usedIdentifier3 = operand3.getIdentifierName();
//					
//					Integer usedSsaVersion2 = operand2.getSsaVersion();
//					Integer usedSsaVersion3 = operand3.getSsaVersion();
//					
//					if(usedIdentifier2.equals(phiUpdatedIdentifier)){
//						if(usedSsaVersion2.equals(leftSsaVersion) || usedSsaVersion2.equals(rightSsaVersion)){
//							operand2.setSsaVersion(phiSsaVersion);
//						}
//					}
//					if(usedIdentifier3.equals(phiSsaVersion)){
//						if(usedSsaVersion3.equals(leftSsaVersion) || usedSsaVersion3.equals(rightSsaVersion)){
//							operand3.setSsaVersion(phiSsaVersion);
//						}
//					}
//					
//					if(newPhiToUseFurther.getIdentifierName().equals(phiUpdatedIdentifier)){
//						phiUpdatedIdentifier = newPhiToUseFurther.getIdentifierName();
//						phiSsaVersion = newPhiToUseFurther.getSsaVersion();
//					}
//					
//					instr.setOperand2(operand2);
//					instr.setOperand3(operand3);
//					
//					updatedInstructions.add(instr);
//				} else {
//					Result usedVar = instr.getOperand1();
//					boolean addedToBlock = false;
//					if(usedVar.getType()!=Type.VARIABLE){//no need to update anything
//						addedToBlock = false;
//					} else if(usedVar.getIdentifierName().equals(phiUpdatedIdentifier)){
//						Integer usedSsaVersion = usedVar.getSsaVersion();
//						if(usedSsaVersion == leftSsaVersion || usedSsaVersion == rightSsaVersion){//we should update
//							usedVar.setSsaVersion(phiSsaVersion);
//							instr.setOperand1(usedVar);
//							updatedInstructions.add(instr);
//							addedToBlock = true;
//						}
//					}
//					if(!addedToBlock){
//						updatedInstructions.add(instr);
//					}
//					
////					Result definedVariable
//				}
//			}
//			currentBasicBlock.setInstructions(updatedInstructions);
//			//current block processed next proceed to its children.
//			
//			for(BasicBlock bbl: currentBasicBlock.getChildren()){
//				queue.add(bbl);
//			}
//			
//		}
//	}

	public void updatePhiFunctionResultsInLoopHeader(BasicBlock whileBlock, Result phiInstrResult, Result leftResult,
			Result rightResult) {
		List<Instruction> instructions = whileBlock.getInstructions();
		List<Instruction> updatedInstructions = new ArrayList<Instruction>();
		
		String phiVarIdentifier = phiInstrResult.getIdentifierName();
		Integer leftSsaVersion = leftResult.getSsaVersion();
		Integer rightSsaVersion = rightResult.getSsaVersion();
		Integer phiSsaVersion = phiInstrResult.getSsaVersion();
		
		for(Instruction instr: instructions){
			boolean addedToBlock = false;
			if(instr.getOperation()==Operation.PHI){
				addedToBlock = false;
			} else {
				Result operand1 = instr.getOperand1();
				if(operand1!=null && operand1.getType()==Type.VARIABLE){//update phi variable only for variables
					String operand1Identifier = operand1.getIdentifierName();
					Integer operand1SsaVersion = operand1.getSsaVersion();
					
					if(operand1Identifier.equals(phiVarIdentifier) && 
							(operand1SsaVersion == leftSsaVersion || operand1SsaVersion == rightSsaVersion)){
						//update instruction only in this case
						
						operand1.setSsaVersion(phiSsaVersion);
						instr.setOperand1(operand1);
						updatedInstructions.add(instr);
						addedToBlock = true;
					}
				} else {
					addedToBlock = false;
				}
			}
			if(!addedToBlock){//no changes. simply add it back
				updatedInstructions.add(instr);
			}
		}
		
		whileBlock.setInstructions(updatedInstructions);
	}

	public Result generateICForSpecialFunctioncall(BasicBlock basicBlock, String functionName, Result param) {
		Instruction specialFunctionInstr = null;
		Result specialFunctionInstrResult = null;
		if(functionName.equals("InputNum")){
			if(param != null) error(PARAMETER_NOT_EXPECTED);
			else{
				specialFunctionInstr = new Instruction();
				specialFunctionInstr.setOperation(Operation.READ);
				addInstrToBasicBlock(basicBlock, specialFunctionInstr);
			}
		} else if(functionName.equals("OutputNum")){
			specialFunctionInstr = new Instruction();
			if(param == null) error(PARAMETER_EXPECTED);
			else{
				specialFunctionInstr.setOperation(Operation.WRITE);
				specialFunctionInstr.setOperand1(param);
				addInstrToBasicBlock(basicBlock, specialFunctionInstr);
			}
		} else if(functionName.equals("OutputNewLine")) {
			if(param != null) error(PARAMETER_NOT_EXPECTED);
			else {
				specialFunctionInstr = new Instruction();
				specialFunctionInstr.setOperation(Operation.WRITENL);
				addInstrToBasicBlock(basicBlock, specialFunctionInstr);
			}
		} else error(NOT_A_SPECIAL_FUNCTION);
		
		specialFunctionInstrResult = new Result();
		specialFunctionInstrResult.setType(Type.INSTRUCTION);
		specialFunctionInstrResult.setInstructionId(specialFunctionInstr.getId());
		
		return specialFunctionInstrResult;
	}

	public void generateICToInitializeVar(String identifier, Function function, BasicBlock basicBlock) {
		Result zero = new Result();
		zero.setType(Type.CONSTANT);
		zero.setValue(0);

		Result varResult = new Result();
		varResult.setType(Type.VARIABLE);
		varResult.setIdentifierName(identifier);

		Instruction initInstr = new Instruction();
		initInstr.setOperation(Operation.MOVE);
		initInstr.setOperand1(zero);
		
		if(function == null){//normal main
			varResult.setSsaVersion(gvm.getLatestSSAVersion(identifier));
			initInstr.setOperand2(varResult);
		} else {//in a function
			varResult.setSsaVersion(function.getLatestSSA(identifier, basicBlock));
			initInstr.setOperand2(varResult);
		}
		addInstrToBasicBlock(basicBlock, initInstr);
	}

	public void generateICForFunctionParams(BasicBlock basicBlock, List<Result> parameters) {
		for(Result param: parameters){
			Instruction paramInstr = new Instruction();
			paramInstr.setOperation(Operation.PARAM);
			paramInstr.setOperand1(param);
			addInstrToBasicBlock(basicBlock, paramInstr);
		}
	}

	public void insertDummyKills(BasicBlock joinBlock, List<Instruction> ifPathStoreKill,
			List<Instruction> elsePathStoreKill) {
		Set<Result> storeKilledArrays = new HashSet<Result>();
		for(Instruction instr: ifPathStoreKill){
			Result killed = instr.getArrayVar();
			storeKilledArrays.add(killed);
		}
		for(Instruction instr: elsePathStoreKill){
			Result killed = instr.getArrayVar();
			storeKilledArrays.add(killed);
		}

		for(Result storeKilledArray: storeKilledArrays){
			Instruction dummyKillInstr = new Instruction();
			dummyKillInstr.setOperation(Operation.DUMMY_KILL);
			dummyKillInstr.setArrayVar(storeKilledArray);
			
			addInstrAtFrontOfBasicBlock(joinBlock, dummyKillInstr);
		}
	}

}
