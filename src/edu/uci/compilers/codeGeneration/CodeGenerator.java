package edu.uci.compilers.codeGeneration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import edu.uci.compilers.cfg.ControlFlowGraph;
import edu.uci.compilers.dataTypes.BasicBlock;
import edu.uci.compilers.dataTypes.BasicBlock.BasicBlockType;
import edu.uci.compilers.dataTypes.ErrorMessage;
import edu.uci.compilers.dataTypes.Instruction;
import edu.uci.compilers.dataTypes.Operation;
import edu.uci.compilers.dataTypes.Result;
import edu.uci.compilers.dataTypes.Result.Type;

public class CodeGenerator {
	
	private static final int STACK_POINTER = 29;
	private static final int FRAME_POINTER = 28;
	private static final int GLOBAL_VARIABLE_MEMORY = 30;
	
	private List<Integer> programCodes;
	private ControlFlowGraph cfg;
	private int codeNumber;
	private Map<Integer, Integer> blockVsCodeNumber;
	private Stack<Integer> branchFixUpLocations;
	
	public CodeGenerator(ControlFlowGraph cfg){
		this.cfg = cfg;
		programCodes = new ArrayList<Integer>();
		blockVsCodeNumber = new HashMap<Integer, Integer>();
	}
	
	public void generateCode(BasicBlock basicBlock){
		generateCodeForBlockBody(basicBlock);
		if(basicBlock.getType() == BasicBlockType.WHILE){
			
		}else if(basicBlock.getType() == BasicBlockType.WHILEBODY){
			
		}else if(basicBlock.getType() == BasicBlockType.IF){
			
		}else if(basicBlock.getType() == BasicBlockType.IF_ELSE_JOIN || 
				basicBlock.getType() == BasicBlockType.IF_JOIN){
			int code = blockVsCodeNumber.get(basicBlock.getBasicBlockId());
			updateBranchCode(OpCode.JSR, code);
			
		}else if(basicBlock.getType() == BasicBlockType.ELSE){
			
		}else if(basicBlock.getType() == BasicBlockType.NORMAL){
			
		}
		
		List<BasicBlock> children = basicBlock.getChildren();
		
		
		for(BasicBlock child : children){
			generateCode(child);
		}
		
	}
	
	public void addCodeToProgram(int code){
		programCodes.add(code);
		codeNumber++;
	}
	
	public void updateCodeIntheProgram(int code, int position){
		if(position < 0 || position >= programCodes.size()){
			error(ErrorMessage.WRONG_FIXUP_LOCATION);
			return;
		}
		programCodes.add(position, code);
	}
	
	public void generateCodeForBlockBody(BasicBlock basicBlock){
		blockVsCodeNumber.put(basicBlock.getBasicBlockId(), codeNumber);
		List<Instruction> instructions = basicBlock.getInstructions();
		for(Instruction instruction : instructions){
			Operation operation = instruction.getOperation();
			switch(operation){
				case ADD:
					addArithmeticCode(OpCode.ADD, OpCode.ADDI, instruction);
					break;
				case SUB:
					addArithmeticCode(OpCode.SUB, OpCode.SUBI, instruction);
					break;
				case MUL:
					addArithmeticCode(OpCode.MUL, OpCode.MULI, instruction);
					break;
				case DIV:
					addArithmeticCode(OpCode.DIV, OpCode.DIVI, instruction);
					break;
				case ADDA:
					addArithmeticCode(OpCode.ADD, OpCode.ADDI, instruction);
					break;
				case CMP:
					addArithmeticCode(OpCode.CMP, OpCode.CMPI, instruction);
					break;
				case BRA:
					addBranchCode(OpCode.JSR, instruction);
				case BEQ:
					addControlCode(OpCode.BEQ, instruction);
					break;
				case BNE:
					addControlCode(OpCode.BNE, instruction);
					break;
				case BLT:
					addControlCode(OpCode.BLT, instruction);
					break;
				case BGE:
					addControlCode(OpCode.BGE, instruction);
					break;
				case BLE:
					addControlCode(OpCode.BLE, instruction);
					break;
				case BGT:
					addControlCode(OpCode.BGT, instruction);
					break;
				case CALL:
					addCallCode(instruction);
					break;
				case RET:
					addReturnCode(instruction);
					break;
				case PARAM:
					addParamCode(instruction);
					break;	
			}
		}
	}
	
	
	private void addBranchCode(OpCode jsr, Instruction instruction) {
		Result operand1 = instruction.getOperand1();
		Integer targetCode = blockVsCodeNumber.get(operand1.getTargetBasicBlock());
		if(targetCode == null){
			targetCode = 0;
		}
		branchFixUpLocations.add(codeNumber);
		//Putting the target code as 0 as a place holder.
		//This will be changed once we fix up the branch locations.
		int code = getF3Format(jsr.getOpcode(), targetCode);
		addCodeToProgram(code);
		
	}
	
	private void updateBranchCode(OpCode jsr, int targetCode){
		int position = branchFixUpLocations.pop();
		int code = getF3Format(jsr.getOpcode(), targetCode);
		updateCodeIntheProgram(code, position);
	}

	private void addParamCode(Instruction instruction) {
		
		
	}

	private void addReturnCode(Instruction instruction) {
		
		
	}

	private void addCallCode(Instruction instruction) {
		
	}

	private void addControlCode(OpCode opCode, Instruction instruction) {
		
	}

	private void addArithmeticCode(OpCode opCode, OpCode immediateOpCode, Instruction instruction) {
		Result operand1 = instruction.getOperand1();
		Result operand2 = instruction.getOperand2();
		int resultRegNo = instruction.getRegNo();
		int code = 0;
		if(operand1.getType() == Type.REGISTER && operand2.getType() == Type.REGISTER){
			code = getF2Format(opCode.getOpcode(), resultRegNo, 
					operand1.getRegisterNumber(), operand2.getRegisterNumber());
			
		}else if(operand1.getType() == Type.REGISTER && operand2.getType() == Type.CONSTANT){
			code = getF1Format(immediateOpCode.getOpcode(), resultRegNo, 
					operand1.getRegisterNumber(), operand2.getValue());
		}else if(operand1.getType() == Type.CONSTANT && operand2.getType() == Type.REGISTER){
			code = getF1Format(immediateOpCode.getOpcode(), resultRegNo, 
					operand2.getRegisterNumber(), operand1.getValue());
		}
		addCodeToProgram(code);
		
	}

	/**
	 * op --> 6 bits
	 * a --> 5 bits
	 * b --> 5 bits
	 * c --> 16 bits
	 * @param op
	 * @param a
	 * @param b
	 * @param c
	 * @return
	 */
	public int getF1Format(int op, int a, int b, int c){
		if(op > 63 || op < 0){
			error(ErrorMessage.OP_CODE_NOT_WITHIN_THE_RANGE);
		}
		if(a < 0 || a > 31 || b < 0 || b > 31){
			error(ErrorMessage.REGISTER_NOT_WITHIN_RANGE);
		}
		if(c > (65536 - 1) || c < 0){
			error(ErrorMessage.LITERAL_NOT_WITHIN_RANGE);
		}
		
		return op << 26 | a << 21 | b << 16 | c;
		
	}
	
	/**
	 * op --> 6 bits
	 * a --> 5 bits
	 * b --> 5 bits
	 * empty --> 11 bits
	 * c --> 5 bits
	 * @param op
	 * @param a
	 * @param b
	 * @param c
	 * @return
	 */
	public int getF2Format(int op, int a, int b, int c){
		if(op > 63 || op < 0){
			error(ErrorMessage.OP_CODE_NOT_WITHIN_THE_RANGE);
		}
		if(a < 0 || a > 31 || b < 0 || b > 31 || c < 0 || c > 31){
			error(ErrorMessage.REGISTER_NOT_WITHIN_RANGE);
		}
		return op << 26 | a << 21 | b << 16 | c;
	}
	
	/**
	 * op --> 6 bits
	 * c --> 26 bits
	 * @param op
	 * @param c
	 * @return
	 */
	public int getF3Format(int op, int c){
		if(op > 63 || op < 0){
			error(ErrorMessage.OP_CODE_NOT_WITHIN_THE_RANGE);
		}
		if(c < 0 || c > 67108864 - 1){
			error(ErrorMessage.C_ABSOLUTE_NOT_WITHIN_THE_RANGE);
		}
		return op << 26 | c;
	}
	
	public void error(String errorMessage){
		System.out.println(errorMessage);
	}
}
