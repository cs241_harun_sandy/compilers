package edu.uci.compilers.registerAllocator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import edu.uci.compilers.cfg.ControlFlowGraph;
import edu.uci.compilers.dataTypes.BasicBlock;
import edu.uci.compilers.dataTypes.BasicBlock.BasicBlockType;
import edu.uci.compilers.dataTypes.Function;
import edu.uci.compilers.dataTypes.GlobalVariableManager;
import edu.uci.compilers.dataTypes.Instruction;
import edu.uci.compilers.dataTypes.InterferenceGraph;
import edu.uci.compilers.dataTypes.Operation;
import edu.uci.compilers.dataTypes.Result;
import edu.uci.compilers.dataTypes.Result.Type;

public class LiveRangeAnalyzer {
	
	private InterferenceGraph interferenceGraph;
	private ControlFlowGraph cfg;
	private GlobalVariableManager gvm;
	
	private boolean backEdge;
	
	public LiveRangeAnalyzer(){
		gvm = GlobalVariableManager.getInstance();
		this.interferenceGraph = new InterferenceGraph();
		this.backEdge = false;
	}
	
	public void setBackEdge(boolean backEdge) {
		this.backEdge = backEdge;
	}
	
	public void setCfg(ControlFlowGraph cfg) {
		this.cfg = cfg;
	}
	public ControlFlowGraph getCfg() {
		return cfg;
	}
	public void setInterferenceGraph(InterferenceGraph interferenceGraph) {
		this.interferenceGraph = interferenceGraph;
	}
	public InterferenceGraph getInterferenceGraph() {
		return interferenceGraph;
	}
	
	public void buildInterferenceGraph(ControlFlowGraph cfg){
		BasicBlock startBasicBlock = cfg.getStartBlock();
		BasicBlock endBasicBlock = cfg.getEndBlock();
//		List<Instruction> liveInstructions = new ArrayList<Instruction>();
		
		Set<Instruction> allInstructionSet = new HashSet<Instruction>();
		Set<Instruction> phiInstructions = new HashSet<Instruction>();
		Set<Result> liveSet = new HashSet<Result>();
		Map<Result, Set<Result>> adjacencyList = new HashMap<Result, Set<Result>>();
		doLiveRangeAnalysis(endBasicBlock, adjacencyList, liveSet, null, allInstructionSet, phiInstructions);
		
		List<Map<Result, Set<Result>>> functionAdjacencyLists = new ArrayList<Map<Result, Set<Result>>>();
		
		Map<String, Function> functions = gvm.getFunctions();
		for(Entry<String, Function> keyVal: functions.entrySet()){
			Function function = keyVal.getValue();
			BasicBlock endFunctionBasicBlock = function.getEndBlock();
			Set<Result> funcLiveSet = new HashSet<Result>();
			Map<Result, Set<Result>> functionAdjacencyList = new HashMap<Result, Set<Result>>();
			doLiveRangeAnalysis(endFunctionBasicBlock, functionAdjacencyList, funcLiveSet, null, allInstructionSet, phiInstructions);
			functionAdjacencyLists.add(functionAdjacencyList);
		}
		
		Map<Result, Set<Result>> adjacencyLists = mergeAllAdjLists(adjacencyList, functionAdjacencyLists);
		
		interferenceGraph.setAdjacencyList(adjacencyLists);
		interferenceGraph.setInstructions(allInstructionSet);
		interferenceGraph.setPhiInstructions(phiInstructions);
		
//		doLiveRangeAnalysisOnBasicBlock(endBasicBlock, liveSet, adjacencyList);
		
//		interferenceGraph.setAdjacencyList(adjacencyList);
	}

	private Map<Result, Set<Result>> mergeAllAdjLists(Map<Result, Set<Result>> adjacencyList,
			List<Map<Result, Set<Result>>> functionAdjacencyLists) {
		Map<Result, Set<Result>> mergedAdjList = new HashMap<Result, Set<Result>>(adjacencyList);
		for(Map<Result, Set<Result>> funcAdjList: functionAdjacencyLists){
			mergedAdjList.putAll(funcAdjList);
		}
		return mergedAdjList;
	}

	private void doLiveRangeAnalysis(BasicBlock basicBlock, Map<Result, Set<Result>> adjacencyList,
			Set<Result> liveSet, BasicBlock upUntilBasicBlock, Set<Instruction> allInstructionSet,
			Set<Instruction> phiInstructions) {
		if(upUntilBasicBlock != null && basicBlock == upUntilBasicBlock)//base condition
			return;

		Set<Result> leftToAdd = new HashSet<Result>();
		Set<Result> rightToAdd = new HashSet<Result>();
		Set<Result> toBeRemoved = new HashSet<Result>();
		
		doLiveRangeAnalysisOnBasicBlock(basicBlock, liveSet, adjacencyList, leftToAdd, rightToAdd, toBeRemoved, 
				allInstructionSet, phiInstructions);
		
		Set<Result> basicBlockLiveSet = basicBlock.getLiveSet();
		for(Result live: liveSet){
			basicBlockLiveSet.add(live);
		}
		basicBlock.setLiveSet(basicBlockLiveSet);
		
		BasicBlockType basicBlockType = basicBlock.getType();
		List<BasicBlock> parents = basicBlock.getParents();
//		BasicBlock lcaBasicBlock = basicBlock.getLcaBasicBlock();
		
		if(basicBlockType == BasicBlockType.IF_JOIN){
			BasicBlock ifPathBlock = getIfPathBlock(parents);
			BasicBlock headerBlock = getElsePathBlock(parents);
			
			Set<Result> leftLiveSet = new HashSet<Result>(liveSet);
			leftLiveSet.addAll(leftToAdd);
			for(Result remove: toBeRemoved){
				leftLiveSet.remove(remove);
			}
			doLiveRangeAnalysis(ifPathBlock, adjacencyList, leftLiveSet, headerBlock, allInstructionSet, phiInstructions);
			
			Set<Result> rightLiveSet = new HashSet<Result>(liveSet);
			rightLiveSet.addAll(rightToAdd);
			for(Result remove: toBeRemoved){
				rightLiveSet.remove(remove);
			}
			
			Set<Result> merger = new HashSet<Result>();
			merger.addAll(leftLiveSet);
			merger.addAll(rightLiveSet);
			doLiveRangeAnalysis(headerBlock, adjacencyList, merger, upUntilBasicBlock, allInstructionSet, phiInstructions);
			
		} else if(basicBlockType == BasicBlockType.IF_ELSE_JOIN){
			BasicBlock ifPathBlock = getIfPathBlock(parents);
			BasicBlock elsePathBlock = getElsePathBlock(parents);
			
			BasicBlock lca = getMergerBlock(basicBlock);
					//getLCA(ifPathBlock, elsePathBlock);//this gives us the up until block - until we finish doing if else
			
			Set<Result> leftLiveSet = new HashSet<Result>(liveSet);
			leftLiveSet.addAll(leftToAdd);
			for(Result remove: toBeRemoved){
				leftLiveSet.remove(remove);
			}
			doLiveRangeAnalysis(ifPathBlock, adjacencyList, leftLiveSet, lca, allInstructionSet, phiInstructions);//assumption leftLive set is updated along the analysis
			
			Set<Result> rightLiveSet = new HashSet<Result>(liveSet);
			rightLiveSet.addAll(rightToAdd);
			for(Result remove: toBeRemoved){
				rightLiveSet.remove(remove);
			}
			doLiveRangeAnalysis(elsePathBlock, adjacencyList, rightLiveSet, lca, allInstructionSet, phiInstructions);
			
			Set<Result> merger = new HashSet<Result>();
			merger.addAll(leftLiveSet);
			merger.addAll(rightLiveSet);
			doLiveRangeAnalysis(lca, adjacencyList, merger, upUntilBasicBlock, allInstructionSet, phiInstructions);
			
		} else if(basicBlockType == BasicBlockType.WHILE){
			BasicBlock bodyBlock = getWhileBodyPathBlock(parents);
			BasicBlock headerBlock = getHeaderPathBlock(parents);
			Set<Result> bodyLiveSet = new HashSet<Result>(liveSet);
			bodyLiveSet.addAll(rightToAdd);
			doLiveRangeAnalysis(bodyBlock, adjacencyList, bodyLiveSet, basicBlock, allInstructionSet, phiInstructions);
			
			Set<Result> headerLiveSet = new HashSet<Result>();
			headerLiveSet.addAll(basicBlock.getLiveSet());
			for(Result remove: toBeRemoved){
				headerLiveSet.remove(remove);
			}
			headerLiveSet.addAll(leftToAdd);
			doLiveRangeAnalysis(headerBlock, adjacencyList, headerLiveSet, upUntilBasicBlock, allInstructionSet, phiInstructions);
			
		} else{//normal
			for(BasicBlock parent: parents){//should have only one parent
				doLiveRangeAnalysis(parent, adjacencyList, basicBlockLiveSet, upUntilBasicBlock, allInstructionSet, phiInstructions);
			}
		}
	}
	private BasicBlock getMergerBlock(BasicBlock basicBlock) {
		return basicBlock.getBottomUpMergerBasicBlock();
	}

	/**
	 * Deprecated. This version of LCA wont work as CFG is not a DAG.
	 * Use getMergerBlock instead. That populates the Merger block while parsing
	 * @param leftBasicBlock
	 * @param rightBasicBlock
	 * @return
	 */
	private BasicBlock getLCA(BasicBlock leftBasicBlock, BasicBlock rightBasicBlock) {
		List<BasicBlock> leftChildren = leftBasicBlock.getChildren();
		if(leftChildren != null && leftChildren.size() > 1){
			for(BasicBlock child: leftChildren){
				if(isReachable(child, rightBasicBlock))
					return leftBasicBlock;
			}
		}
		List<BasicBlock> parents = leftBasicBlock.getParents();
		BasicBlock lca = null;
		for(BasicBlock parent: parents){
			lca = getLCA(parent, rightBasicBlock);
			if(lca != null)
				break;
		}
		return lca;
	}

	private boolean isReachable(BasicBlock root, BasicBlock target) {
		Set<BasicBlock> visitedBasicBlocks = new HashSet<BasicBlock>();
		return dfs(root, target, visitedBasicBlocks);
	}
	
	private boolean dfs(BasicBlock root, BasicBlock target, Set<BasicBlock> visitedBasicBlocks){
		if(root == null) return false;
		if(root == target) return true;
		if(visitedBasicBlocks.contains(root)) return false;
		visitedBasicBlocks.add(root);
		List<BasicBlock> children = root.getChildren();
		boolean reachable = false;
		for(BasicBlock child: children){
			reachable = dfs(child, target, visitedBasicBlocks);
			if(reachable) break;
		}
		return reachable;
	}

	private BasicBlock getWhileBodyPathBlock(List<BasicBlock> parents) {
		BasicBlock whileBodyPathBlock = null;
		for(BasicBlock parent: parents){ // if the last instr is uncond branch, then its the while body path
			List<Instruction> instructions = parent.getInstructions();
			if(instructions.size() == 0) continue;
			Instruction instr = instructions.get(instructions.size()-1);
			Operation op = instr.getOperation();
			if(op == Operation.BRA){
				whileBodyPathBlock = parent;
				break;
			}
		}
		return whileBodyPathBlock;
	}

	private BasicBlock getHeaderPathBlock(List<BasicBlock> parents) {// assumption - all basic blocks have at most 2 parents
		BasicBlock headerPathBlock = null;
		BasicBlock whileBodyPathBlock = getWhileBodyPathBlock(parents);
		for(BasicBlock parent: parents){
			if(parent != whileBodyPathBlock){
				headerPathBlock = parent;
				break;
			}
		}
		return headerPathBlock;
	}

	private BasicBlock getElsePathBlock(List<BasicBlock> parents) {
		BasicBlock elsePathBlock = null;
		BasicBlock ifPathBlock = getIfPathBlock(parents);
		for(BasicBlock parent: parents){
			if(parent != ifPathBlock){
				elsePathBlock = parent;
				break;
			}
		}
		return elsePathBlock;
	}

	private BasicBlock getIfPathBlock(List<BasicBlock> parents) {
		BasicBlock ifPathBlock = null;
		for(BasicBlock parent: parents){
			List<Instruction> instructions = parent.getInstructions();
			if(instructions.size() == 0){
				ifPathBlock = parent;
				break;
			} else {
				Instruction instr = instructions.get(instructions.size()-1);//if final instruction is an uncond branch(BRA) then this is an if
				Operation op = instr.getOperation();
				if(op == Operation.BRA){
					ifPathBlock = parent;
					break;
				}
			}
		}
		return ifPathBlock;
	}

	private boolean shouldAdd(Instruction instr) {
		Operation op = instr.getOperation();
		if(isBranchInstr(instr)) return false;
		if(op == Operation.CALL || op == Operation.PROPAGATE || op == Operation.END) return false;//other misc instructions
		if(isConditionalBranch(op)) return false;
		return true;
	}

	private boolean isConditionalBranch(Operation op) {
		if(op == Operation.BRA || op == Operation.BNE || op == Operation.BEQ || op == Operation.BLE
				|| op == Operation.BLT || op == Operation.BGE || op == Operation.BGT)
			return true;
		return false;
	}

	private boolean isBranchInstr(Instruction instr) {
		Operation op = instr.getOperation();
		if(op == Operation.BRA || op == Operation.BNE || op == Operation.BEQ || op == Operation.BLT 
				|| op == Operation.BLT || op == Operation.BGE || op == Operation.BGT) return true;
		return false;
	}

	private void doLiveRangeAnalysisOnBasicBlock(BasicBlock basicBlock, Set<Result> liveSet, Map<Result, Set<Result>> adjList,
			Set<Result> leftToAdd, Set<Result> rightToAdd, Set<Result> toBeRemoved, Set<Instruction> allInstructionSet,
			Set<Instruction> phiInstructions) {
		List<Instruction> instructions = basicBlock.getInstructions();
		for(int index = instructions.size()-1; index >= 0; index --){
			Instruction instr = instructions.get(index);
			allInstructionSet.add(instr);
			Operation op = instr.getOperation();
			if(!shouldAdd(instr)) continue;
			if(op == Operation.PHI){//assumption - all phis occur at the beginning of a basic block
				phiInstructions.add(instr);
				
				Result phiResult = instr.getOperand1();
				Result phiLeftResult = instr.getOperand2();
				Result phiRightResult = instr.getOperand3();
				
				if(phiLeftResult.getType() != Type.CONSTANT)
					leftToAdd.add(phiLeftResult);
				if(phiRightResult.getType() != Type.CONSTANT)
					rightToAdd.add(phiRightResult);
				toBeRemoved.add(phiResult);
				
				liveSet.remove(phiResult);
				populateAdjacencyMatrix(phiResult, liveSet, adjList);
				
				if(phiLeftResult.getType() != Type.CONSTANT)
					liveSet.add(phiLeftResult);
				if(phiRightResult.getType() != Type.CONSTANT)
					liveSet.add(phiRightResult);
//				populateAdjacencyMatrix(phiResult, liveSet, adjList);//for all x belonging to live add edge i <-> x
				//TODO - doubt what should the current basic block' live set be in case we encounter phis
				continue;
			}
			
			//for i: op j k
			Result inRes = new Result();//inRes is "i"
			inRes.setType(Type.INSTRUCTION);
			inRes.setInstructionId(instr.getId());
			if(liveSet.contains(inRes)){//live = live - {i}////live is liveSet
				liveSet.remove(inRes);
			}
			populateAdjacencyMatrix(inRes, liveSet, adjList); //for all x belonging to live add edge i <-> x
			
			Result operand1 = instr.getOperand1();
			Result operand2 = instr.getOperand2();
			Result operand3 = instr.getOperand3();
			
			if(operand1 != null && shouldAddOperand(operand1)) 
				liveSet.add(operand1);//live + {j, k}
			if(operand2 != null && shouldAddOperand(operand2)) 
				liveSet.add(operand2);
			if(operand3 != null && shouldAddOperand(operand3)) 
				liveSet.add(operand3);
			
		}
	}

	private boolean shouldAddOperand(Result operand) {
		if(operand.getType() == Type.CONSTANT || operand.getType() != Type.ARRAYVAR || operand.getType() != Type.BASEADDRESS
				|| operand.getType() != Type.DUMMY || operand.getType() != Type.FRAMEPOINTER)
			return false;
		return true;
	}

	private void populateAdjacencyMatrix(Result inRes, Set<Result> liveSet, Map<Result, Set<Result>> adjList) {
		Set<Result> liveSetCopy = deepCopy(liveSet);
		for(Result result: liveSet){
			Set<Result> forwardEdges = adjList.get(result);
//			if(val != null)	val.add(inRes);
			if(forwardEdges == null) forwardEdges = new HashSet<Result>();
			forwardEdges.add(inRes);
			adjList.put(result, forwardEdges);
			if(backEdge){
				Set<Result> reverseEdges = adjList.get(inRes); //comment/uncomment the following 4 lines to ignore/add reverse edges
				if(reverseEdges == null) reverseEdges = new HashSet<Result>(); 
				reverseEdges.add(result);
				adjList.put(inRes, reverseEdges);
			}
		}
		if(liveSet.isEmpty()){
			if(!adjList.containsKey(inRes)){
				adjList.put(inRes, new HashSet<Result>());//doesn't interact with anything
			}
		}
//		adjList.get(inRes).addAll(liveSetCopy);
//		adjList.put(inRes, liveSetCopy);
	}

	private Set<Result> deepCopy(Set<Result> liveSet) {
		Set<Result> copy = new HashSet<Result>();
		for(Result result: liveSet){
			copy.add(result);
		}
		return copy;
	}	
}
