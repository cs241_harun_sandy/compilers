package edu.uci.compilers.registerAllocator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.Set;

import edu.uci.compilers.cfg.ControlFlowGraph;
import edu.uci.compilers.dataTypes.Function;
import edu.uci.compilers.dataTypes.GlobalVariableManager;
import edu.uci.compilers.dataTypes.Instruction;
import edu.uci.compilers.dataTypes.InterferenceGraph;
import edu.uci.compilers.dataTypes.PhiFunctionManager;
import edu.uci.compilers.dataTypes.Result;
import edu.uci.compilers.dataTypes.Result.Type;

public class RegisterAllocator {

	private static final int PHYSICAL_REGISTER_COUNT = 8;// for the DLX architecture. Architecture dependend
	private static final int MAX_TOTAL_REGISTERS = 65536;
	private static int numColorsUsed = 0;
	private Map<Result, Integer> registerMap;
	private Map<Result, List<Result>> clusterMap;
	private ControlFlowGraph cfg;
	private GlobalVariableManager gvm;
	
	public void setCfg(ControlFlowGraph cfg) {
		this.cfg = cfg;
	}
	public ControlFlowGraph getCfg() {
		return cfg;
	}
	public RegisterAllocator(){
		registerMap = new HashMap<Result, Integer>();
		clusterMap = new HashMap<Result, List<Result>>();
		gvm = GlobalVariableManager.getInstance();
	}
	private void setClusterMap(Map<Result, List<Result>> clusterMap) {
		this.clusterMap = clusterMap;
	}
	private Map<Result, List<Result>> getClusterMap() {
		return clusterMap;
	}
	public void allocateRegisters(InterferenceGraph interferenceGraph){
		Set<Instruction> instructions = interferenceGraph.getInstructions();
		Map<Result, Set<Result>> adjList = interferenceGraph.getAdjacencyList();
		doClusteringOnPhiInstructions(interferenceGraph, adjList);
		
		colorGraph(interferenceGraph);
		//allocate the registers
//		printRegisterMap();
		for (Instruction instruction : instructions) {	
			updateInstructionsToRegisters(instruction);
		}
		removePhiFunctions();
	}

	private void removePhiFunctions() {
		PhiFunctionManager phiManager = new PhiFunctionManager();
		phiManager.setCfg(cfg);
		phiManager.removePhiFunctions();
	}
	private void doClusteringOnPhiInstructions(InterferenceGraph interferenceGraph, Map<Result, Set<Result>> adjList) {
		Set<Instruction> phiInstructions = interferenceGraph.getPhiInstructions();
		
		for(Instruction phiInstruction: phiInstructions){
			Result phiResult = phiInstruction.getOperand1();
			Result phiLeftResult = phiInstruction.getOperand2();
			Result phiRightResult = phiInstruction.getOperand3();
			Set<Result> cluster = new HashSet<Result>();
			cluster.add(phiResult);
			boolean interfereLeft = false, interfereRight = false;
			if(phiLeftResult.getType() != Type.CONSTANT){
				Set<Result> leftNeighbors = adjList.get(phiLeftResult);
				for(Result x: cluster){
					if(leftNeighbors.contains(x)){
						interfereLeft = true;
						break;
					}
				}
				if(!interfereLeft){
					cluster.add(phiLeftResult);
				}
			}
			if(phiRightResult.getType() != Type.CONSTANT){
				Set<Result> rightNeighbors = adjList.get(phiRightResult);
				for(Result x: cluster){
					if(rightNeighbors.contains(x)){
						interfereRight = true;
						break;
					}
				}
				if(!interfereRight){
					cluster.add(phiRightResult);
				}
			}
			cluster.remove(phiResult);
			if(!cluster.isEmpty()){
				addToClusterMap(phiResult, cluster);
			}
		}
		updateAdjListAccordingToClustering(adjList);
	}

	private void addToClusterMap(Result phiResult, Set<Result> cluster) {
		if(clusterMap.containsKey(phiResult)){
			clusterMap.get(phiResult).addAll(cluster);
		} else{
			List<Result> clusterList = new ArrayList<Result>();
			for(Result add: cluster){
				clusterList.add(add);
			}
			clusterMap.put(phiResult, clusterList);
		}
	}

	private void updateAdjListAccordingToClustering(Map<Result, Set<Result>> adjList) {
		//remove same result in set of results
		for(Entry<Result, List<Result>> keyVal: clusterMap.entrySet()){
			Result key = keyVal.getKey();
			List<Result> val = keyVal.getValue();
			val.remove(key);
		}
		unionFindToCluster();
		//now clustering is done
//		printClusterMap();
		Map<Result, List<Result>> cluster = getClusterMap();
		for(Entry<Result, List<Result>> keyVal: cluster.entrySet()){
			List<Result> clusteredResults = keyVal.getValue();
			for(Result clusteredResult: clusteredResults){
				adjList.remove(clusteredResult);
				for(Entry<Result, Set<Result>> adjKeyVal: adjList.entrySet()){
					Set<Result> adjacentNodes = adjKeyVal.getValue();
					adjacentNodes.remove(clusteredResult);
				}
			}
		}
	}
	private void unionFindToCluster() {
		Map<Integer, List<Result>> union = new HashMap<Integer, List<Result>>();
		Map<Result, Integer> find  = new HashMap<Result, Integer>();
		int newGroup = 1;
		for(Entry<Result, List<Result>> keyVal: clusterMap.entrySet()){
			Result key = keyVal.getKey();
			List<Result> val = keyVal.getValue();
			
			if(val.size() == 1){
				Result res = val.get(0);
				Integer group1 = find.get(key);
				Integer group2 = find.get(res);
				if(group1 == null && group2 == null){//first time
					List<Result> toAdd = new ArrayList<Result>();
					toAdd.add(key);
					toAdd.add(res);
					union.put(newGroup, toAdd);
					find.put(key, newGroup);
					find.put(res, newGroup);
					newGroup++;
				} else if(group1 == null && group2 != null){
					union.get(group2).add(key);
					find.put(key, group2);
				} else if(group1 != null && group2 == null){
					union.get(group1).add(res);
					find.put(res, group1);
				} else{
					if(group1 == group2){//no problem
						
					} else{//we have to merge
						merge(group1, group2, newGroup, union, find);
						newGroup++;
					}
				}
			} else {//size is 2
				Result res1 = val.get(0);
				Result res2 = val.get(1);
				Integer group1 = find.get(key);
				Integer group2 = find.get(res1);
				Integer group3 = find.get(res2);
				if(group1 == null && group2 == null && group3 == null){
					List<Result> toAdd = new ArrayList<Result>();
					toAdd.add(key);
					toAdd.add(res1);
					toAdd.add(res2);
					union.put(newGroup, toAdd);
					for(Result r: toAdd){
						find.put(r, newGroup);
					}
					newGroup++;
				} else if(group1 == null && group2 == null && group3 != null){
					List<Result> toAdd = new ArrayList<Result>();
					toAdd.add(key);
					toAdd.add(res1);
					union.get(group3).addAll(toAdd);
					for(Result res: toAdd){
						find.put(res, group3);
					}
				} else if(group1 == null && group2 != null && group3 == null){
					List<Result> toAdd = new ArrayList<Result>();
					toAdd.add(key);
					toAdd.add(res2);
					union.get(group2).addAll(toAdd);
					for(Result res: toAdd){
						find.put(res, group2);
					}
				} else if(group1 == null && group2 != null && group3 != null){
					//add group1 to group2 first
					union.get(group2).add(key);
					find.put(key, group2);
					//now check for union find
					if(group2 != group3){
						merge(group2, group3, newGroup, union, find);
						newGroup++;
					}
				} else if(group1 != null && group2 == null && group3 == null){
					List<Result> toAdd = new ArrayList<Result>();
					toAdd.add(res1);
					toAdd.add(res2);
					union.get(group1).addAll(toAdd);
					for(Result res: toAdd){
						find.put(res, group1);
					}
				} else if(group1 != null && group2 == null && group3 != null){
					//add group2 to group1 first
					union.get(group1).add(res1);
					find.put(res1, group1);
					//now check for union find
					if(group1 != group3){
						merge(group1, group3, newGroup, union, find);
						newGroup++;
					}
				} else if(group1 != null && group2 != null && group3 == null){
					//add group3 to group1 first
					union.get(group1).add(res2);
					find.put(res2, group1);
					//now check for union find
					if(group1 != group2){
						merge(group1, group2, newGroup, union, find);
						newGroup++;
					}
				} else{// all are not null
					if(group1 != group2){
						merge(group1, group2, newGroup, union, find);
						newGroup++;
					}
					Integer updatedGroup = find.get(key);
					if(updatedGroup != group3){
						merge(updatedGroup, group3, newGroup, union, find);
						newGroup++;
					}
				}
			}
		}
		Map<Result, List<Result>>updatedClusterMap = new HashMap<Result, List<Result>>();
		for(Entry<Integer, List<Result>> keyVal: union.entrySet()){
			List<Result> clusterGroup = keyVal.getValue();
			Result clusterParent = clusterGroup.get(0);//assumption
			clusterGroup.remove(0);
			updatedClusterMap.put(clusterParent, clusterGroup);
		}
		setClusterMap(updatedClusterMap);
	}

	private void merge(Integer group1, Integer group2, Integer newGroup, Map<Integer, List<Result>> union,
			Map<Result, Integer> find){
		List<Result> toAdd = new ArrayList<Result>();
		toAdd.addAll(union.get(group1));
		toAdd.addAll(union.get(group2));
		
		union.remove(group1);
		union.remove(group2);
		union.put(newGroup, toAdd);
		for(Result newRes: toAdd){
			find.put(newRes, newGroup);
		}

	}

	public void printClusterMap(){
		System.out.println("ClusterMap: ");
		for(Entry<Result, List<Result>> keyVal: clusterMap.entrySet()){
			System.out.println(keyVal.getKey() + ": " + keyVal.getValue());
		}
		System.out.println("ClusterMap over");
	}
	public void printRegisterMap() {
		for(Entry<Result, Integer> resultReg: registerMap.entrySet()){
			Result result = resultReg.getKey();
			Integer register = resultReg.getValue();
			System.out.println(result + ": " + "R_" + register);
		}
	}

	private void updateInstructionsToRegisters(Instruction instruction) {
		int instructionId = instruction.getId();
		Result operand1 = instruction.getOperand1();
		Result operand2 = instruction.getOperand2();
		Result operand3 = instruction.getOperand3();
		
		Result instructionRes = new Result();
		instructionRes.setType(Type.INSTRUCTION);
		instructionRes.setInstructionId(instructionId);
		
		boolean instResDone = false, op1Done = false, op2Done = false, op3Done = false;
		if(operand1 == null) op1Done = true;
		if(operand2 == null) op2Done = true;
		if(operand3 == null) op3Done = true;
	
		if(!op1Done && operand1.getRegisterNumber() != -1){
			op1Done = true;
			operand1.setType(Type.REGISTER);
		}
		if(!op2Done && operand2.getRegisterNumber() != -1){
			op2Done = true;
			operand2.setType(Type.REGISTER);
		}
		if(!op3Done && operand3.getRegisterNumber() != -1){
			op3Done = true;
			operand3.setType(Type.REGISTER);
		}
		
		for(Entry<Result, Integer> keyVal: registerMap.entrySet()){
			Result key = keyVal.getKey();
			if(!instResDone && instructionRes.equals(key)){
				instructionRes.setRegisterNumber(keyVal.getValue());
				instructionRes.setType(Type.REGISTER);
				instruction.setRegNo(keyVal.getValue());
				instResDone = true;
			}
			if(!op1Done && operand1.equals(key)){
				operand1.setRegisterNumber(keyVal.getValue());
				operand1.setType(Type.REGISTER);
				instruction.setOperand1(operand1);
				op1Done = true;
			}
			if(!op2Done && operand2.equals(key)){
				operand2.setRegisterNumber(keyVal.getValue());
				operand2.setType(Type.REGISTER);
				instruction.setOperand2(operand2);
				op2Done = true;
			}
			if(!op3Done && operand3.equals(key)){
				operand3.setRegisterNumber(keyVal.getValue());
				operand3.setType(Type.REGISTER);
				instruction.setOperand3(operand3);
				op3Done = true;
			}
			if(instResDone && op1Done && op2Done && op3Done) break;
		}
	}

	private void colorGraph(InterferenceGraph iGraph) {//updates register numbers alone
		Result node = chooseNode(iGraph);
		Set<Result> neighbors = iGraph.getNeighbors(node);
		iGraph.removeNode(node);
		if(!iGraph.isEmpty()){
			colorGraph(iGraph);
		}
		iGraph.addNode(node, neighbors);
		chooseAndSetColorForNode(node, neighbors);
	}

	private void chooseAndSetColorForNode(Result node, Set<Result> neighbors){
		Set<Integer> colorsTaken = new HashSet<Integer>();
		colorsTaken.add(0);//special register
		
		for(Result neighbor: neighbors){
			Integer regNo = registerMap.get(neighbor);
			colorsTaken.add(regNo);
//			colorsTaken.add(neighbor.getRegisterNumber());
		}
		
		for(Integer color = 0; color < MAX_TOTAL_REGISTERS; color++){
			if(!colorsTaken.contains(color)){
//				node.setRegisterNumber(color);
				Result putResutReg = new Result(node);
				registerMap.put(putResutReg, color);
				List<Result> cluster = clusterMap.get(node);
				if(cluster != null && cluster.size() > 0){
					for(Result x: cluster){
						Result xNew = new Result(x);
						registerMap.put(xNew, color);
					}
				}
				break;
			}
		}
	}

	private Result chooseNode(InterferenceGraph iGraph) {
		Map<Result, Set<Result>> adjList = iGraph.getAdjacencyList();
		//choose the node with least neighbors
		Result returnNode = null;
		int numNeighbors_min = PHYSICAL_REGISTER_COUNT + 1;
		for (Entry<Result, Set<Result>> entry : adjList.entrySet()) {
			int size = entry.getValue().size();
			if(size < numNeighbors_min){
				returnNode = entry.getKey();
				numNeighbors_min = size;
				break;
			}
		}
		if(returnNode == null){//choose an arbitrary node. we can apply heuristics here! currently node with min edges
			numNeighbors_min = adjList.size() + 1;
			for(Entry<Result, Set<Result>> keyVal: adjList.entrySet()){
				int size = Integer.MIN_VALUE;
				if(size < numNeighbors_min){
					returnNode = keyVal.getKey();
					numNeighbors_min = size;
					break;
				}
			}
		}
		return returnNode;
	}
	
}
